package org.group.poker.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Date;
import java.util.Scanner;

import org.group.poker.server.exception.CroupierGameAlreadyRunningException;

/**
 * Server singleton, implements runnable so once started
 * does not suspend main application.
 * Also provides static logger.
 * @author dotPo
 */
public enum Server implements Runnable {
	INSTANCE;
	
	/**
	 * @see org.group.poker.server.ConnectionDaemon
	 */
	private ConnectionDaemon _daemon;
	
	/**
	 * Server.
	 */
	private ServerSocket _serverSocket;
	
	/**
	 * Player manager.
	 */
	private Croupier _croupier;
	
	/**
	 * Server port.
	 */
	private int _port;	
	
	/**
	 * Initial cash each player has.
	 */
	private float _initialMoney;
	
	/**
	 * Minimal number of players required to start.
	 */
	private int _minPlayers;
	
	/**
	 * Maximum number of players required to start.
	 */
	private int _maxPlayers;
	
	/**
	 * Default port to use when not specified.
	 */
	public final static int DEFAULT_PORT = 9090;
	/**
	 * Determines whether server should log all messages or non-debug only.
	 */
	private static boolean logAlways = false;
	
	private Server() {
		_initialMoney = 100.0f;
		_port = DEFAULT_PORT;
		_minPlayers = 2;
	}
	
	/**
	 * @return
	 * Server socket.
	 */
	public ServerSocket get_serverSocket() {return _serverSocket;}
	
	/**
	 * @return
	 * Connection daemon.
	 */
	public ConnectionDaemon get_connectionDaemon() {return _daemon;}
	
	/**
	 * @return
	 * Player manager.
	 */
	public Croupier get_croupier() {return _croupier;}
	
	/**
	 * NOTE: Later on we may allow to assign port number only before server started.
	 * @param port
	 * Port number to start server on.
	 */
	public void set_port(final int port) {_port = port;}
	
	/**
	 * @return
	 * Port number on which server is starting or running.
	 */
	public int get_port() {return _port;}
	
	public void set_cash(float amt) {_initialMoney = amt;}
	public float get_cash() {return _initialMoney;}

	public void set_minPlayers(int min) {
		_minPlayers = (min > 2 ? min <= 10 ? min : 10 : 2);
	}
	public int get_minPlayers() {return _minPlayers;}
	
	public void set_maxPlayers(int max) {
		_maxPlayers = (max > 2 ? max <= 10 ? max : 10 : 2);
	}
	public int get_maxPlayers() {return _maxPlayers;}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			_serverSocket = new ServerSocket(get_port());
		} catch (IOException e) {
			log("Failed to open server on port: " + _port, true);
			return;
		}
		
		_croupier = new Croupier();
		_daemon = new ConnectionDaemon(_serverSocket, _croupier);
		_daemon.start();
		
		final Scanner stdin = new Scanner(System.in);
		while(stdin.hasNext()) {
			parse(stdin.nextLine());
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		stdin.close();
	}	

	/**
	 * Parse admin's input. Eg. ADDBOT().
	 * @param nextLine
	 */
	private void parse(final String nextLine) {
		if(nextLine.toUpperCase().equals("START")) {
			try {
				_croupier.start_game();
				Server.log("Game started!", true);
			} catch (CroupierGameAlreadyRunningException e) {
				Server.log("Game is already running!", true);
			}
		}
		else if(nextLine.toUpperCase().equals("FIXED")) {
			_croupier.set_mode(GameMode.FIXED);
		}
		else if(nextLine.toUpperCase().equals("POT")) {
			_croupier.set_mode(GameMode.POT);
		}
		else if(nextLine.toUpperCase().equals("NOLIMIT")) {
			_croupier.set_mode(GameMode.NOLIMIT);
		}
		else {
			String[] cmd = nextLine.split(" ", 2);
			if(cmd.length == 2) {
				if(cmd[0].toUpperCase().equals("AMT")) {
					_croupier.set_fixedAmount(
							Float.parseFloat(cmd[1]));
				}
				else if(cmd[0].toUpperCase().equals("MAX")) {
					_croupier.set_maxRaises(
							Integer.parseInt(cmd[1]));
				}
			}
		}
	}

	/**
	 * Determine whether debug messages should be displayed.
	 * @param shouldLog
	 */
	public static void setLogging(final boolean shouldLog) {
		logAlways = shouldLog;
	}
	
	/**
	 * Log message to standard output. Log as debug message if
	 * always is false or as persistent one if true.
	 * @param msg
	 * @param always
	 */
	public static void log(final String msg, final boolean always) {
		if(logAlways || always)
			System.out.println(
					("[" + new Date()).toString() + 
					(always ? "] " : " - DEBUG] ") + msg);
	}
	
	/**
	 * Log debug message. See also {@link Server#log(String, boolean)}.
	 * @param msg
	 */
	public static void log(final String msg) {
		log(msg, false);
	}
	
}
