package org.group.poker.server;

import java.util.ArrayList;
import java.util.List;

import org.group.poker.server.exception.PotNoSuchPlayerException;

/**
 * Pot is created once player all-ins or round finishes.
 * All pots are values that certain players can win during game.
 * @author dot
 */
public class Pot {
	private List<Integer> _players;
	private float _value;
	
	public Pot() {
		this(new ArrayList<Integer>());
	}
	
	public Pot(List<Integer> players) {
		_players = players;
		_value = 0.0f;
	}
	
	public void add_player(Integer id) {
		_players.add(id);
	}
	public void remove_player(Integer id) throws
			PotNoSuchPlayerException {
		if(!_players.contains(id))
			throw new PotNoSuchPlayerException();
		_players.remove((Object) id);
	}	
	public List<Integer> get_playersInPot() {
		return _players;
	}
	public float get_potValue() {
		return _value;
	}
	public void set_potValue(float value) {
		_value = value;
	}
	public void raise_potValue(float value) {
		set_potValue(get_potValue() + value);
	}
}
