package org.group.poker.server.exception;

public class CroupierUnableToStartGameException extends Exception {

	public CroupierUnableToStartGameException(String string) {
		super(string);
	}

}
