package org.group.poker.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.group.poker.server.exception.CroupierInvalidActionException;
import org.group.poker.server.exception.CroupierInvalidAmountException;
import org.group.poker.server.exception.CroupierInvalidValueException;
import org.group.poker.server.exception.CroupierListenerExistenceException;
import org.group.poker.server.exception.CroupierMaxRaisesReachedException;
import org.group.poker.server.exception.CroupierNoGameInProgressException;
import org.group.poker.server.exception.CroupierNotUniqueNameException;
import org.group.poker.server.exception.CroupierPlayerExistenceException;
import org.group.poker.server.exception.CroupierPlayerLockedException;
import org.group.poker.server.exception.CroupierTooSmallBetException;
import org.group.poker.server.exception.CroupierUnknownException;
import org.group.poker.server.exception.UserInsufficientFoundsException;

import org.group.poker.server.utility.Commands;
import org.group.poker.server.utility.Counter;

/**
 * <pre>
 * Messages format (each message is ended by '\n':
 * 
 * Server	->	Client
 * _______________________
 * MESSAGE "string" - Broadcasts plain message. 
 *                    Doesn't need to be interpreted by client.
 * STATUS "string" - Broadcasts formated message, 
 *                   that can be interpreted by client
 *  
 * Client	->	Server
 * _______________________
 * JOIN "string" - Informs server that player wants to 
 *                 join a game with nickname.
 * LEAVE - Informs server that player leaves.
 * READY - client is ready to start game.
 * BET "amount" - Bets given amount. Includes cases of betting blinds.
 * RAISE "amount" - Raise your bet by given amount over someone else bet.
 * CALL - Raise your bet to highest so far.
 * FOLD - Resignate from turn.
 * ALLIN - Place all your money as bet.
 * BROADCAST "string" - Send message to all players.
 * STATUS - Requests current table status from server.
 * 							
 * -- "amount" only positive real values are accepted.
 * -- "which" can be either BETS, CARDS.
 * -- "string" is descriptive, non-parseable message.
 * 
 * Everything is also available in {@link org.group.poker.server.utility.Commands}
 * </pre>
 * 
 * @author dotPo
 */
public class ConnectionPipe  {
	/**
	 * Unique id of this pipe/user.
	 */
	private final int _id;	//NOPMD
	
	/**
	 * Player manager.
	 */
	private Croupier _croupier;
	
	/**
	 * Pipe output stream. Which means, what's written in here goes
	 * to user.
	 */
	private OutputStream _socketOut;	//NOPMD
	
	/**
	 * Pipe input stream. Users scribbles.
	 */
	private InputStream _socketIn;	//NOPMD
	
	/**
	 * Writer which uses Pipe's output stream.
	 */
	private PrintWriter _writer;	//NOPMD
	
	/**
	 * Reader which uses Pipe's input stream.
	 */
	private BufferedReader _reader;	//NOPMD

	private PipeWrite _writerThread;
	private PipeRead _readerThread;
	
	/**
	 * Client's socket. 
	 */
	private final Socket _client;	//NOPMD
	
	/**
	 * @param connection
	 * Socket at which user is connected to us.
	 * @param _croupier2 
	 */
	public ConnectionPipe(
			final Socket connection,
			Croupier croupier) {
		super();
		_croupier = croupier;
		_client = connection;
		_id = Counter.getNext();
	}
	
	/**
	 * @return
	 * Unique ID of this Pipe.
	 */
	public int get_id() {return _id;}
	
	public Croupier get_croupier() {
		return _croupier;
	}
	
	public void send_message(String msg) {
		synchronized (_writerThread) {
			_writerThread.send_message(msg);
		}
	}
	
	public void start() {
		try {
			_croupier.add_listener(_id, this);
		} catch (CroupierListenerExistenceException e1) {
			Server.log("Unable to add listener! Stopping...", true);
			return;
		}
		try {
			_socketOut = _client.getOutputStream();
			_socketIn = _client.getInputStream();
		} catch (IOException e) {
			Server.log("Unable to open i/o streams with client!");
		}
		
		_writer = new PrintWriter(_socketOut, true);
		_reader = new BufferedReader(new InputStreamReader(_socketIn));
		
		_writerThread = new PipeWrite(_writer);
		_writerThread.start();
		_writerThread.send_message("MESSAGE Connected to server!");
		
		_readerThread = new PipeRead(_reader, this);
		_readerThread.start();
	}
	
	/**
	 * Parse client's input.
	 * @param msg
	 * Message following the rules.
	 * @return
	 * Server's response.
	 */
	
	public String parse(final String msg) {
		final String cmd[] = msg.split(" ", 2);
		
		Commands cas = Commands.fromName(cmd[0]);
		
		if(cas == null)
			return "MESSAGE Invalid command!";
		
		User user = _croupier.get_user(_id);
		
		switch(cas) {
		case ALLIN:
			try {
				_croupier.allin(_id);
				return "MESSAGE You placed bet with all your money! Your"
						+ " total bet is now: " + user.get_bet();
			} catch (CroupierNoGameInProgressException e2) {
				return "MESSAGE No game in progress!";
			} catch (CroupierPlayerExistenceException e2) {
				return "MESSAGE You need to join lobby!";
			} catch (CroupierUnknownException e2) {
				return "MESSAGE Invalid active user!";
			} catch (CroupierListenerExistenceException e2) {
				return "MESSAGE Connection proglems!";
			} catch (CroupierPlayerLockedException e2) {
				return "MESSAGE Wait for your turn!";
			} catch (CroupierInvalidValueException 
					| UserInsufficientFoundsException e2) {
				return "MESSAGE You don't have moeny to do so!";
			}
		case BET:
			if(cmd.length != 2)
				return "MESSAGE You need to specify amount!";
			try {
				_croupier.bet(_id, cmd[1]);
				return "MESSAGE You placed bet!";
			} catch (NumberFormatException e1) {
				return "MESSAGE Invalid format! Remember, that 2.0 is ok,"
						+ " but 2.0$ isn't. No coloring!";
			} catch (CroupierPlayerExistenceException e1) {
				return "MESSAGE First join the lobby!";
			} catch (CroupierNoGameInProgressException e1) {
				return "MESSAGE No game in progress!";
			} catch (CroupierInvalidActionException e1) {
				return "MESSAGE You can only bet as your first action in"
						+ " round. Now you can pick from RAISE, ALLIN,"
						+ " FOLD or CHECK";
			} catch (CroupierListenerExistenceException e1) {
				return "MESSAGE You are not recognized by server!";
			} catch (UserInsufficientFoundsException e1) {
				return "MESSAGE You don't have enought money in wallet!"
						+ " You have: " + user.get_walletContents();
			} catch (CroupierUnknownException e1) {
				return "MESSAGE You are not in game!";
			} catch (CroupierPlayerLockedException e1) {
				return "MESSAGE Wait for your turn!";
			}
		case BROADCAST:
			return null;
		case CALL:
			try {
				_croupier.call(_id);
				return "MESSAGE Your bet is now: " + user.get_bet();
			} catch (UserInsufficientFoundsException e1) {
				return "MESSAGE You don't have enought money!";
			} catch (CroupierNoGameInProgressException e1) {
				return "MESSAGE No game in progress!";
			} catch (CroupierPlayerExistenceException e1) {
				return "MESSAGE You need to join lobby first!";
			} catch (CroupierUnknownException e1) {
				return "MESSAGE Invalid active user!";
			} catch (CroupierListenerExistenceException e1) {
				return "MESSAGE Connection problems!";
			} catch (CroupierPlayerLockedException e1) {
				return "MESSAGE Wait for your turn!";
			} catch (CroupierInvalidValueException e1) {
				return "MESSAGE Your bet is already highest!"
						+ " Consider CHECK!";
			}
		case CHECK:
			try {
				_croupier.check(_id);
				return "MESSAGE Checked!";
			} catch (CroupierNoGameInProgressException e2) {
				return "MESSAGE No game in progress!";
			} catch (CroupierPlayerExistenceException e2) {
				return "MESSAGE You are not in lobby!";
			} catch (CroupierUnknownException e2) {
				return "MESSAGE Invalid active user!";
			} catch (CroupierListenerExistenceException e2) {
				return "MESSAGE Connection problems!";
			} catch (CroupierPlayerLockedException e2) {
				return "MESSAGE Wait for your turn!";
			} catch (CroupierInvalidValueException e2) {
				return "MESSAGE You cannot check!";
			}
		case FOLD:
			try {
				_croupier.fold(_id);
				return "MESSAGE You folded! Now you have to wait"
						+ " untill this round ends!";
			} catch (CroupierNoGameInProgressException e1) {
				return "MESSAGE No game in progress!";
			} catch (CroupierPlayerExistenceException e1) {
				return "MESSAGE You need to join lobby first!";
			} catch (CroupierUnknownException e1) {
				return "MESSAGE Invalid active user!";
			} catch (CroupierListenerExistenceException e1) {
				return "MESSAGE Connection problems!";
			} catch (CroupierPlayerLockedException e1) {
				return "MESSAGE Wait for your turn!";
			}
		case JOIN:
			if(cmd.length != 2)
				return "MESSAGE You need to supply your name!";
			try {
				_croupier.add_user(_id, cmd[1]);
				return "MESSAGE You entered lobby as " + cmd[1] + "."
						+ " Type ready when you are ready!";
			} catch (CroupierPlayerExistenceException e) {
				return "MESSAGE You are already in lobby!";
			} catch (CroupierListenerExistenceException e) {
				return "MESSAGE You shouldn't be able ot do that!";
			} catch (CroupierNotUniqueNameException e) {
				return "MESSAGE Name is already taken!";
			}
		case LEAVE:
			try {
				_croupier.remove_user(_id);
				return "MESSAGE You left  lobby!";
			} catch (CroupierPlayerExistenceException e) {
				return "MESSAGE You are not in lobby!";
			}
		case RAISE:
			if(cmd.length != 2)
			return "MESSAGE You need to specify amount!";
			try {
				_croupier.raise(_id, cmd[1]);
				return "MESSAGE You've raised your bet! It is now:"
						+ user.get_bet();
			} catch (NumberFormatException e1) {
				return "MESSAGE Invalid format! Please send only"
						+ "number with no additions (2.0 instead"
						+ " of 2$).";
			} catch (CroupierPlayerExistenceException e1) {
				return "MESSAGE You need to join lobby first!";
			} catch (CroupierNoGameInProgressException e1) {
				return "MESSAGE No game in progress!";
			} catch (CroupierInvalidActionException e1) {
				return "MESSAGE You need to BET first!";
			} catch (CroupierListenerExistenceException e1) {
				return "MESSAGE Connection problems!";
			} catch (UserInsufficientFoundsException e1) {
				return "MESSAGE You don't have enought money!";
			} catch (CroupierUnknownException e1) {
				return "MESSAGE Invalid active user!";
			} catch (CroupierPlayerLockedException e1) {
				return "MESSAGE Wait for your turn!";
			} catch (CroupierInvalidAmountException e1) {
				if(e1.get_mode() == GameMode.FIXED) {
					return "MESSAGE There's bet limit, equal"
							+ _croupier.get_game().get_fiexedAmount();
				}
				else {
					return "MESSAGE You cannot raise by more than thre's"
							+ " money in pot!";
				}
			} catch (CroupierMaxRaisesReachedException e1) {
				return "MESSAGE You used all your raises!";
			} catch (CroupierTooSmallBetException e) {
				return "MESSAGE You must raise by more than 0!";
			}
		case READY:
			try {
				_croupier.ready(_id);
				return "MESSAGE You are now ready! There are total of "
				+ Integer.toString(
						_croupier.get_usersWithStatus(
						Croupier.FLAG_READY).size())
				+ " ready users!";
			} catch (CroupierPlayerExistenceException e) {
				return "MESSAGE You are not in lobby!";
			}
		case STATUS:
			try {
				return _croupier.status(_id);
			} catch (CroupierUnknownException e) {
				return "MESSAGE Cards haven't been handed yet!";
			} catch (CroupierPlayerExistenceException e) {
				return "MESSAGE You need to join lobby before asking for status!";
			}
		case HELP:
			return help();
		default:
			return "MESSAGE Unknown command!";	
		}
	}	
	
	public String help() {
		return "MESSAGE "
				+ "Availible commands:\n"
				+ "join [nickname] - join lobby with"
				+ " given nickname\n"
				+ "leave - leave lobby\n"
				+ "ready - declare as ready to play\n"
				+ "status - get status of a game\n"
				+ "bet [amount] - bet given amount\n"
				+ "raise [amount] - raise your bet by given amount\n"
				+ "call - raise your bet to highest of"
				+ " the other players\n"
				+ "check - if you have highest bet, you can wait "
				+ "without losing your right to play\n"
				+ "fold - resignate from playing\n"
				+ "allin - go all in, place all your money"
				+ " as bet\n";
	}
	
	public void finish() {
			try {
				_croupier.remove_listener(_id);
			} catch (CroupierListenerExistenceException e) {
				// It means user's already absent.
			}
	}
}
