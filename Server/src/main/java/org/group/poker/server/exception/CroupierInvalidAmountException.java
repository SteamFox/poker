package org.group.poker.server.exception;

import org.group.poker.server.GameMode;

public class CroupierInvalidAmountException extends Exception {
	private GameMode _mode;	
	
	public CroupierInvalidAmountException(GameMode mode) {
		_mode = mode;
	}
	
	public GameMode get_mode() {
		return _mode;
	}

}
