package org.group.poker.server;

/**
 * Static access point.
 * @author dotPo
 */
public final class ServerExec {

	/*
	 * Make class static and not constructible.
	 */
	private ServerExec() {}
	
	/**
	 * Access point for executable.
	 * @param args
	 * Command line arguments.
	 */
	public static void main(final String[] args) { 	
		Server.setLogging(true);
		Server.log("Server starting...", true);
		Thread serverThread = new Thread(Server.INSTANCE, "ServerThread");
		int port = 9090;
		if(args.length > 1)
			try {
				port = Integer.parseInt(args[0]);
			} catch(NumberFormatException e) {
				Server.log("Unable to parse server port arg.", true);
			}
		Server.INSTANCE.set_port(port);
		serverThread.start();				
	}

}
