package org.group.poker.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Daemon running in background accepting new connections.
 * @author dotPo
 */
public class ConnectionDaemon extends Thread {	//NOPMD
	
	/**
	 * Server socket link. Links to {@link Server#get_serverSocket()}
	 */
	private ServerSocket _serverSocket;	//NOPMD
	
	private Croupier _croupier;
	/**
	 * List of active connections.
	 */
	private List<ConnectionPipe> _connections;	//NOPMD
		
	public ConnectionDaemon(
			ServerSocket instance,
			Croupier croupier) {
		_serverSocket = instance;
		_croupier = croupier;
		_connections = new ArrayList<ConnectionPipe>();
	}

	/**
	 * @return
	 * List of active connections.
	 */
	public List<ConnectionPipe> get_currentConnections() {
		synchronized(_connections) {
			return _connections;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		while(true) {
			try {
				handleNewConnection(_serverSocket.accept());
			} catch (IOException e) {
				Server.log("Unable to handle new incoming connection.");
			}
		}
	}
	
	/**
	 * @param connection
	 * Client socket to handle.
	 */
	public void handleNewConnection(final Socket connection) {
		final ConnectionPipe pipe = new ConnectionPipe(
				connection,
				_croupier);
		synchronized(_connections) {			
			_connections.add(pipe);			
		}
		pipe.start();
	}
	
	/**
	 * @param connection
	 * Connection to delete from memory.
	 */
	public void killConnection(final ConnectionPipe connection) {
		synchronized (_connections) {
			_connections.remove(connection);
		}
	}
}
