package org.group.poker.server.utility;

import java.util.ArrayList;

public class CircularList<E> extends ArrayList<E> {

    /**
	 * Generated serial version ID.
	 */
	private static final long serialVersionUID = 3961421709985764977L;

	@Override
    public E get(int index) {
        return super.get(index % size());
    }
}