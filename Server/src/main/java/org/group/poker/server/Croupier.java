package org.group.poker.server;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.group.poker.croupier.Card;
import org.group.poker.server.exception.CroupierGameAlreadyRunningException;
import org.group.poker.server.exception.CroupierInvalidActionException;
import org.group.poker.server.exception.CroupierInvalidAmountException;
import org.group.poker.server.exception.CroupierInvalidValueException;
import org.group.poker.server.exception.CroupierListenerExistenceException;
import org.group.poker.server.exception.CroupierMaxRaisesReachedException;
import org.group.poker.server.exception.CroupierNoGameInProgressException;
import org.group.poker.server.exception.CroupierNotUniqueNameException;
import org.group.poker.server.exception.CroupierPlayerExistenceException;
import org.group.poker.server.exception.CroupierPlayerLockedException;
import org.group.poker.server.exception.CroupierTooSmallBetException;
import org.group.poker.server.exception.CroupierUnknownException;
import org.group.poker.server.exception.UserInsufficientFoundsException;

public class Croupier {
	// FIELDS
	
	private Map<Integer, ConnectionPipe> _listeners;
	private Map<Integer, User> _users;
	private Game _game;
	
	private boolean _gameInProgress;
	private GameMode _mode;
	private float _fixedAmount;
	private int _maxRaises;
	
	public static final int FLAG_WAITING = 0x1;
	public static final int FLAG_LOCKED = 0x2;
	public static final int FLAG_READY = 0x4;
	public static final int FLAG_INGAME = 0x8;
	
	// C's & D's
	
	/**
	 * Creates new instance. This is not initializing game!
	 */
 	public Croupier() {
		_listeners = new HashMap<Integer, ConnectionPipe>();
		_users =  new HashMap<Integer, User>();
		set_mode(GameMode.NOLIMIT);
		set_fixedAmount(10.0f);
		set_maxRaises(5);
	}
	
	// GET
	
	/**
	 * @return
	 * Listeners map, containing all connected listeners.
	 */
	public Map<Integer, ConnectionPipe> get_listeners() {
		return _listeners;
	}
	
	/**
	 * Listener getter. Safe version, throws on listener
	 * absence.
	 * @param id
	 * Id of listener owner.
	 * @return
	 * Listener if found.
	 * @throws CroupierListenerExistenceException
	 * When there's no listener with such id.
	 */
	public ConnectionPipe get_listener_s(Integer id) throws 
			CroupierListenerExistenceException {
		if(!_listeners.containsKey(id))
			throw new CroupierListenerExistenceException();
		return _listeners.get(id);
	}
	
	/**
	 * Listener getter.
	 * @param id
	 * Id of listener owner.
	 * @return
	 * Listener if found, null otherwise.
	 */
	public ConnectionPipe get_listener(Integer id) {
		return _listeners.get(id);
	}
			
	/**
	 * @return
	 * Map with all users in lobby.
	 */
	public Map<Integer, User> get_users() {
		return _users;
	}
	
	/**
	 * User getter. Safe version.
	 * @param id
	 * User's id.
	 * @return
	 * Such user, if exists.
	 * @throws CroupierPlayerExistenceException
	 * When user with such id doesn't exist.
	 */
	public User get_user_s(Integer id) throws 
			CroupierPlayerExistenceException {
		if(!_users.containsKey(id))
			throw new CroupierPlayerExistenceException();
		return _users.get(id);
	}
	
	/**
	 * User getter.
	 * @param id
	 * User's id.
	 * @return
	 * Such user if found, null otherwise.
	 */
	public User get_user(Integer id) {
		return _users.get(id);
	}
	
	/**
	 * User getter. Safe version.
	 * @param name
	 * User's name.
	 * @return
	 * Such user if found.
	 * @throws CroupierPlayerExistenceException
	 * When user with such id doesn't exist.
	 */
	public User get_user_s(String name) throws 
			CroupierPlayerExistenceException {
		synchronized (_users) {
			for(User user : _users.values()) {
				if(user.get_name().equals(name))
					return user;
			}
		}
		throw new CroupierPlayerExistenceException();
	}
	
	/**
	 * User getter.
	 * @param name
	 * User's name.
	 * @return
	 * Such user if found, null otherwise.
	 */
	public User get_user(String name) {
		synchronized (_users) {
			for(User user : _users.values()) {
				if(user.get_name().equals(name))
					return user;
			}
		}
		return null;
	}
	
	/**
	 * @return
	 * Game thread instance.
	 */
	public Game get_game() {
			return _game;		
	}
	
	/**
	 * @return
	 * Whether game  has started yet or not.
	 */
	public boolean is_gameInProgress() {
		return _gameInProgress;
	}
	
	/**
	 * Returns list of user that satisfy certain conditions
	 * specified by flags. Allowed flags are public fields in
	 * Croupier looking like FLAG_%.
	 * @param flag
	 * @return
	 * List of certain users.
	 */
	public List<User> get_usersWithStatus(int flag) {
		List<User> users = new ArrayList<User>();
		
		Map<Integer, User> lobby = get_users();
		synchronized (lobby) {
			for(User user : lobby.values()) {
				boolean add = true;
				if((flag & FLAG_WAITING) > 0)
					if(!user.get_waiting())
						add = false;
				if((flag & FLAG_READY) > 0)
					if(!user.get_ready())
						add = false;
				if((flag & FLAG_INGAME) > 0)
					if(!user.get_playing())
						add = false;
				if((flag & FLAG_LOCKED) > 0)
					if(!user.get_lock())
						add = false;
				if(add) users.add(user);
			}
		}		
		return users;
	}
	
	/**
	 * @return
	 * List of users that are in game.
	 */
	public List<User> get_inGameUsers() {
		return get_usersWithStatus(FLAG_INGAME);
	}
		
	//SET
	
	public void set_mode(GameMode mode) {
			_mode = mode;
	}
	
	public void set_fixedAmount(float fixedAmount) {
		_fixedAmount = fixedAmount;
	}
	
	public void set_maxRaises(int maxRaises) {
		_maxRaises = maxRaises;
	}
	
	/**
	 * Sets game status to either in progress or not.
	 * @param gameInProgress
	 */
	public void set_gameInProgress(boolean gameInProgress) {
		_gameInProgress = gameInProgress;
	}
	
	public void set_users(Map<Integer, User> users) {
		_users = users;
	}

	//CORE
	/**
	 * Starts game. It initialized {@link Croupier#_game}
	 * so it's no longer null and then starts it.
	 * @throws CroupierGameAlreadyRunningException 
	 * When some game is already running.
	 */
	public void start_game() throws 
			CroupierGameAlreadyRunningException {
		if(is_gameInProgress())
			throw new CroupierGameAlreadyRunningException();
		_game = new Game(this, _mode, _fixedAmount, _maxRaises);
		set_gameInProgress(true);
		_game.start();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// Well, it isn't necessary for it to sleep. But it should.
		}
	}
	
	/**
	 * Sends stop signal to game and waits till it's finished.
	 * @throws CroupierNoGameInProgressException
	 * When there's no game in progress.
	 * @throws InterruptedException 
	 * When cannot interrupt game thread.
	 */
	public void stop_game() throws 
			CroupierNoGameInProgressException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		_game.end();
		_game = null;
		set_gameInProgress(false);
	}
	
	/**
	 * Interrupts game loop and waits for thread to join.
	 * Brute force method compared to {@link Croupier#stop_game()}.
	 * @throws CroupierNoGameInProgressException
	 * When there's no game in progress.
	 * @throws InterruptedException 
	 * When cannot interrupt game thread.
	 */
	public void interrupt_game() throws 
			CroupierNoGameInProgressException, 
			InterruptedException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		_game.interrupt();
		_game = null;
		set_gameInProgress(false);
	}
	
	//METHODS
	
	/**
	 * Add new listener once client connects to server.
	 * @param id
	 * User's id.
	 * @param pipe
	 * Connection pipe server and client are using.
	 * @throws CroupierListenerExistenceException
	 * When you are already connected on such id.
	 */
	public void add_listener(Integer id, ConnectionPipe pipe) throws 
			CroupierListenerExistenceException {
		if(_listeners.containsKey(id))
			throw new CroupierListenerExistenceException();
		_listeners.put(id, pipe);
	}
	
	/**
	 * Remove a listener. Usually called once client disconnects.
	 * @param id
	 * User's id.
	 * @throws CroupierListenerExistenceException
	 * When you are not connected.
	 */
	public void remove_listener(Integer id) throws
			CroupierListenerExistenceException {
		synchronized (_listeners) {
			if(!_listeners.containsKey(id))
				throw new CroupierListenerExistenceException();
			_listeners.remove(id);			
		}
		synchronized (_users) {
			if(_users.containsKey(id))
				_users.remove(id);			
		}
	}
	
	/**
	 * Join alias. Add new user to lobby.
	 * @param id
	 * User's id.
	 * @param name
	 * User's nickname.
	 * @throws CroupierPlayerExistenceException
	 * When you are already in lobby.
	 * @throws CroupierListenerExistenceException
	 * When there's no such connection.
	 * @throws CroupierNotUniqueNameException 
	 * When there's user with same name.
	 */
	public void add_user(Integer id, String name) throws 
			CroupierPlayerExistenceException, 
			CroupierListenerExistenceException,
			CroupierNotUniqueNameException {
		synchronized (_listeners) {
			if(!_listeners.containsKey(id))
				throw new CroupierListenerExistenceException();
		}
		synchronized (_users) {
			if(_users.containsKey(id))
				throw new CroupierPlayerExistenceException();
			if(get_user(name) != null)
				throw new CroupierNotUniqueNameException();
			_users.put(id, new User(id, name));
		}		
	}
	
	/**
	 * Leave alias. Remove an user form lobby.
	 * @param id
	 * User's id.
	 * @throws CroupierPlayerExistenceException
	 * When you are not present in lobby.
	 */
	public void remove_user(Integer id) throws 
			CroupierPlayerExistenceException {
		synchronized (_users) {
			if(!_users.containsKey(id))
				throw new CroupierPlayerExistenceException();
			_users.remove(id);
		}		
	}
	
	/**
	 * User declared himself ready to play.
	 * @param id
	 * User's id.
	 * @throws CroupierPlayerExistenceException
	 * When user doesn't exist.
	 */
	public void ready(Integer id) throws 
			CroupierPlayerExistenceException {
		synchronized(_users) {
			if(!_users.containsKey(id))
				throw new CroupierPlayerExistenceException();
			_users.get(id).ready();
		}
	}
	
	/**
	 * User declared himself not-ready to play.
	 * @param id
	 * User's id.
	 * @throws CroupierPlayerExistenceException
	 * When user doesn't exist.
	 */
	public void unready(Integer id) throws 
			CroupierPlayerExistenceException {
		synchronized(_users) {
			if(!_users.containsKey(id))
				throw new CroupierPlayerExistenceException();
			_users.get(id).not_ready();
		}
	}
	
	public void bet(Integer id, String amount) throws 
			CroupierPlayerExistenceException, 
			CroupierNoGameInProgressException, 
			CroupierInvalidActionException,
			NumberFormatException, 
			CroupierListenerExistenceException, 
			UserInsufficientFoundsException, 
			CroupierUnknownException, 
			CroupierPlayerLockedException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		synchronized (_users) {
			User requestee = _users.get(id);
			if(requestee == null)
				throw new CroupierPlayerExistenceException();
			User spectre = _game.get_activeUser();
			if(spectre == null)
				throw new CroupierUnknownException();
			ConnectionPipe pipe = _listeners.get(id);
			if(pipe == null)
				throw new CroupierListenerExistenceException();
			if(spectre.get_id() != requestee.get_id()) 
				throw new CroupierPlayerLockedException();
			if(requestee.get_bet() > 0)
				throw new CroupierInvalidActionException();
			float bet = Float.parseFloat(amount);
			requestee.raise_bet(bet);
			requestee.set_visited(true);
			get_game().raise_pot(id, bet);
			get_game().add_toTotalBet(bet);
			get_game().next();
		}
	}
	
	public void raise(Integer id, String amount) throws 
			CroupierPlayerExistenceException, 
			CroupierNoGameInProgressException, 
			CroupierInvalidActionException,
			NumberFormatException, 
			CroupierListenerExistenceException, 
			UserInsufficientFoundsException, 
			CroupierUnknownException, 
			CroupierPlayerLockedException, 
			CroupierInvalidAmountException, 
			CroupierMaxRaisesReachedException, 
			CroupierTooSmallBetException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		synchronized (_users) {
			User requestee = _users.get(id);
			if(requestee == null)
				throw new CroupierPlayerExistenceException();
			User spectre = _game.get_activeUser();
			if(spectre == null)
				throw new CroupierUnknownException();
			ConnectionPipe pipe = _listeners.get(id);
			if(pipe == null)
				throw new CroupierListenerExistenceException();
			if(spectre.get_id() != requestee.get_id()) 
				throw new CroupierPlayerLockedException();
			if(requestee.get_bet() == 0)
				throw new CroupierInvalidActionException();
			float bet = Float.parseFloat(amount);
			
			if(get_game().get_mode() == GameMode.POT) {
				if(bet > get_game().get_totalBet())
					throw new CroupierInvalidAmountException(
							GameMode.POT);
			}
			else if(get_game().get_mode() == GameMode.FIXED) {
				if(bet > get_game().get_fiexedAmount())
					throw new CroupierInvalidAmountException(
							GameMode.FIXED);
				if(requestee.get_raises() > get_game().get_maxRaises())
					throw new CroupierMaxRaisesReachedException();
			}
			float high = 0.0f;
			int count = 0;
			for(User user : get_game().get_activeUsers()) {
				if(user.get_bet() > high) {
					high = user.get_bet();
					count++;
				}
			}
			if(requestee.get_bet() + bet == high)
				throw new CroupierTooSmallBetException();
			requestee.raise_bet(bet);
			requestee.set_visited(true);
			requestee.set_raises(requestee.get_raises() + 1);
			get_game().raise_pot(id, bet);
			get_game().add_toTotalBet(bet);
			get_game().next();
		}
	}
	
	public void allin(Integer id) throws 
			CroupierNoGameInProgressException, 
			CroupierPlayerExistenceException, 
			CroupierUnknownException, 
			CroupierListenerExistenceException, 
			CroupierPlayerLockedException, 
			CroupierInvalidValueException, 
			UserInsufficientFoundsException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		synchronized (_users) {
			User requestee = _users.get(id);
			if(requestee == null)
				throw new CroupierPlayerExistenceException();
			User spectre = _game.get_activeUser();
			if(spectre == null)
				throw new CroupierUnknownException();
			ConnectionPipe pipe = _listeners.get(id);
			if(pipe == null)
				throw new CroupierListenerExistenceException();
			if(spectre.get_id() != requestee.get_id()) 
				throw new CroupierPlayerLockedException();
			if(requestee.get_walletContents() == 0.0f)
				throw new CroupierInvalidValueException();
			float bet = requestee.get_walletContents();
			requestee.raise_bet(bet);
			requestee.lock();
			requestee.set_visited(true);
			get_game().raise_pot(id, bet);
			get_game().push_newPot();
			get_game().add_toTotalBet(bet);
			get_game().next();
		}
	}
	
	public void call(Integer id) throws 
			UserInsufficientFoundsException, 
			CroupierNoGameInProgressException, 
			CroupierPlayerExistenceException, 
			CroupierUnknownException, 
			CroupierListenerExistenceException, 
			CroupierPlayerLockedException, 
			CroupierInvalidValueException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		synchronized (_users) {
			User requestee = _users.get(id);
			if(requestee == null)
				throw new CroupierPlayerExistenceException();
			User spectre = _game.get_activeUser();
			if(spectre == null)
				throw new CroupierUnknownException();
			ConnectionPipe pipe = _listeners.get(id);
			if(pipe == null)
				throw new CroupierListenerExistenceException();
			if(spectre.get_id() != requestee.get_id()) 
				throw new CroupierPlayerLockedException();
			float high = 0.0f;
			for(User user : get_game().get_activeUsers()) {
				if(user.get_bet() > high) 
					high = user.get_bet();
			}
			float bet = high - requestee.get_bet();
			if(high <= 0) 
				throw new CroupierInvalidValueException();
			requestee.raise_bet(bet);
			requestee.set_visited(true);
			get_game().raise_pot(id, bet);
			get_game().add_toTotalBet(bet);
			get_game().next();
		}
	}
	
	public void check(Integer id) throws 
			CroupierNoGameInProgressException, 
			CroupierPlayerExistenceException, 
			CroupierUnknownException, 
			CroupierListenerExistenceException, 
			CroupierPlayerLockedException, 
			CroupierInvalidValueException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		synchronized (_users) {
			User requestee = _users.get(id);
			if(requestee == null)
				throw new CroupierPlayerExistenceException();
			User spectre = _game.get_activeUser();
			if(spectre == null)
				throw new CroupierUnknownException();
			ConnectionPipe pipe = _listeners.get(id);
			if(pipe == null)
				throw new CroupierListenerExistenceException();
			if(spectre.get_id() != requestee.get_id()) 
				throw new CroupierPlayerLockedException();
			float high = 0.0f;
			for(User user : get_game().get_activeUsers()) {
				if(user.get_bet() > high) 
					high = user.get_bet();
			}
			if(high != requestee.get_bet()) 
				throw new CroupierInvalidValueException();
			requestee.set_visited(true);
			get_game().next();
		}
	}
	
	public void fold(Integer id) throws 
			CroupierNoGameInProgressException, 
			CroupierPlayerExistenceException, 
			CroupierUnknownException, 
			CroupierListenerExistenceException, 
			CroupierPlayerLockedException {
		if(!is_gameInProgress())
			throw new CroupierNoGameInProgressException();
		synchronized (_users) {
			User requestee = _users.get(id);
			if(requestee == null)
				throw new CroupierPlayerExistenceException();
			User spectre = _game.get_activeUser();
			if(spectre == null)
				throw new CroupierUnknownException();
			ConnectionPipe pipe = _listeners.get(id);
			if(pipe == null)
				throw new CroupierListenerExistenceException();
			if(spectre.get_id() != requestee.get_id()) 
				throw new CroupierPlayerLockedException();
			requestee.set_bet(0.0f);
			requestee.set_waiting(true);
			requestee.set_visited(true);
			get_game().next();
		}
	}
	
	public String status(Integer id) throws 
			CroupierUnknownException, 
			CroupierPlayerExistenceException {
		if(!is_gameInProgress())
			return "STATUS 0";
		
		StringBuilder status = new StringBuilder("STATUS ");
		
		User user = get_user(id);
		if(user == null || !user.get_playing())
			status.append("[null],[null];");
		else {
			List<Card> cards = get_game().get_cards().get(id);
			if(cards == null || cards.size() == 0)
				throw new CroupierUnknownException();
			for(int i = 0; i < cards.size(); i++) {
				status.append(cards.get(i).GetDesc());
				status.append((i == cards.size() - 1) ? ';' : ',');
			}
		}
		
		// So far we have STATUS [cardA],[cardB];
		
		List<User> users = get_inGameUsers();
		for(int i = 0; i < users.size(); i++) {
			user = users.get(i);
			status.append(user.get_name() + ",");
			status.append(user.get_walletContents() + ",");
			status.append(user.get_bet() + ",");
			status.append((user.get_lock()) ? "A," : "N,"); // A for went all in, N for can still play
			status.append((user.get_waiting()) ? "F," : "N,"); //F for folded, N for still can play
			status.append((user.get_id() == 
					get_game().get_activeUser().get_id()) ? "*," : "N,"); // * for being active user, N when not.
			status.append((user.get_id() == get_game().get_dealerUser().get_id()) ? "D" : "N");
			status.append((i == users.size() - 1) ? ";" : ",");
		}
		
		List<Card> publicCards = get_game().get_publicCards();
		int howMany = 0;
		if(get_game().get_roundNumber() > 0) {
			howMany = 2 + get_game().get_roundNumber();
		}
		for(int i = 0; i < howMany; i++) {
			status.append(publicCards.get(i).GetDesc());
			status.append((i == howMany - 1) ? ";" : ",");
		}
		
		status.append(get_game().get_totalBet());
		
		return status.toString();
	}
}