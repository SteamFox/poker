package org.group.poker.server;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class PipeWrite extends Thread {

	private PrintWriter _writer;
	private List<String> _backlog;
	
	public PipeWrite(PrintWriter pw) {
		_writer = pw;
		_backlog = new ArrayList<String>();
	}
	
	@Override
	public void run() {
		while(true) {
			synchronized(_backlog) {
				int size = _backlog.size();
				if(size > 0) {
					for(int it = size - 1; it > -1; it--) {
						String s = _backlog.get(it);
						_writer.println(s);
						_backlog.remove(it);
					}
				}
			}
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// NULL
			}
		}
	}	

	public void send_message(String message) {
		synchronized (_backlog) {
			_backlog.add(message);
		}
	}
}
