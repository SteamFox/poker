package org.group.poker.server;

import org.group.poker.server.exception.UserInsufficientFoundsException;

/**
 * User class containing all client data
 * needed by croupier.
 * @author dot
 */
public class User {
	
	/**
	 * Player's nickname.
	 */
	private String _name;
	/**
	 * Player's id.
	 */
	private Integer _id;
	
	/**
	 * Current player's bet.
	 */
	private float _bet;
	/**
	 * Contents of player wallet (bet isn't included);
	 */
	private float _wallet;
	
	/**
	 * States whether player is in game or not. 
	 */
	private boolean _playing;
	/**
	 * States whether player can bet or do other actions
	 * or not.
	 */
	private boolean _locked;
	/**
	 * States whether player is waiting for next round
	 * and has folded or not.
	 */
	private boolean _waiting;
	/**
	 * States whether user is ready.
	 */
	private boolean _ready;
	/**
	 * Used only in fixed-limit game. Determines how many times
	 * did user raised so far. This value cannot exceed certain
	 * value, which is managed by Game.
	 */
	private int _raises;
	/**
	 * States whether this player has performed an action
	 * this round.
	 */
	private boolean _visited; 
	
	/**
	 * Creates new user.
	 * @param name
	 * Nickname of that user.
	 */
	public User(Integer id, String name) {
		_id = id;
		_name = name;
		_bet = 0.0f;
		_wallet = 0.0f;
		_playing = false;
		_waiting = false;
		_locked = false;
	}
	
	/**
	 * @return
	 * Id of user.
	 */
	public int get_id() {
		return _id;
	}
	
	/**
	 * Change name of user.
	 * @param name
	 */
	public void rename(String name) {
		synchronized (_name) {
			_name = name;
		}
	}
	
	/**
	 * @return
	 * Nickname of user.
	 */
	public String get_name() {
		return _name;
	}
	
	/**
	 * @see User#get_walletContents()
	 * @param amount
	 */
	public void set_walletContents(float amount) {
		_wallet = amount;
	}
	
	/**
	 * Get contents of your wallet.
	 * It's worth noting that wallet money is separated from
	 * placed bet. Therefore user total money = bet + wallet. 
	 * @return
	 */
	public float get_walletContents() {
		return _wallet;
	}
	
	/**
	 * Raise bet by given amount.
	 * @param bet
	 * Amount to raise.
	 * @throws UserInsufficientFoundsException
	 * Thrown if there are insufficient founds in wallet.
	 */
	public void raise_bet(float bet) throws 
			UserInsufficientFoundsException {
		if(_wallet - bet > 0) {
			_bet += bet;
			_wallet -= bet;
		}
		else
			throw new UserInsufficientFoundsException();
	}
	
	/**
	 * Place all contents of your wallet as bet
	 * and then lock yourself from performing
	 * other actions.
	 */
	public void all_in() {
		_bet += _wallet;
		_wallet = 0.0f;
		lock();
	}
	
	/**
	 * @see User#get_lock()
	 */
	public void lock() {
		_locked = true;
	}
	
	/**
	 * @see User#get_lock()
	 */
	public void unlock() {
		_locked = false;
	}
	
	/**
	 * Determines whether user is locked from performing
	 * actions. Locked user cannot bet but he can 
	 * win part of pot. Locked user  is someone who has
	 * gone all-in.
	 * @return
	 */
	public boolean get_lock() {
		return _locked;
	}
	
	/**
	 * @see User#get_playing()
	 * @param playing
	 */
	public void set_playing(boolean playing) {
		_playing = playing;
	}
	
	/**
	 * Determines whether user is in game or not.
	 * @return
	 * Is user playing.
	 */
	public boolean get_playing() {
		return _playing;
	}
	
	/**
	 * @see User#get_waiting()
	 * @param waiting
	 */
	public void set_waiting(boolean waiting) {
		_waiting = waiting;
	}
	
	/**
	 * Determines whether user's waiting for
	 * next turn or not.
	 * When user's waiting, he can only check status
	 * on the table. He can not partake in betting nor
	 * he can receive part of any pot.
	 * @return
	 * Is user waiting.
	 */
	public boolean get_waiting() {
		return _waiting;
	}
	
	/**
	 * @see User#get_ready()
	 */
	public void ready() {
		_ready = true;
	}
	
	/**
	 * @see User#get_ready()
	 */
	public void not_ready() {
		_ready = false;
	}
	
	/**
	 * States whether user can join a game or not.
	 * @return
	 * Is user ready.
	 */
	public boolean get_ready() {
		return _ready;
	}
	
	/**
	 * @return
	 * Users bet.
	 */
	public float get_bet() {
		return _bet;
	}
	
	public void set_bet(float bet) {
		_bet = bet;
	}

	/**
	 * @see User#get_raises()
	 * @param raises
	 */
	public void set_raises(int raises) {
		this._raises = raises;
	}
	
	/**
	 * @see User#get_raises()
	 * Increases raises by one.
	 */
	public void raised() {
		set_raises(get_raises() + 1);
	}
	
	/**
	 * Resets raises (sets it to 0).
	 */
	public void reset_raises() {
		set_raises(0);
	}

	/**
	 * @return
	 * Amount of raises user done this round.
	 */
	public int get_raises() { 
		return _raises;
	}
	
	/**
	 * @see User#get_visited()
	 * @param _visited
	 */
	public void set_visited(boolean _visited) {
		this._visited = _visited;
	}	
	
	/**
	 * @return
	 * Has player performed action this turn or not.
	 */
	public boolean get_visited() {
		return _visited;
	}
}
