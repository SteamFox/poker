package org.group.poker.server.utility;

/**
 * TODO: Split it into client commands and server commands.
 * Enumerated helper for commands that are
 * acceptable for server.
 * @author dotPo
 */
public enum Commands {
	JOIN("JOIN"),
	LEAVE("LEAVE"),
	READY("READY"),
	BET("BET"),
	RAISE("RAISE"),
	CALL("CALL"),
	FOLD("FOLD"),
	ALLIN("ALLIN"),
	BROADCAST("BROADCAST"),
	STATUS("STATUS"),
	CHECK("CHECK"),
	HELP("HELP");
	
	/**
	 * String value of enumeration.
	 */
	private final String _val;
	
	/**
	 * Create new command.
	 * @param val
	 * String assigned to that command.
	 */
	Commands(final String val) {
		_val = val;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return _val;
	}
	
	/**
	 * I'm not sure how it does it's thing
	 * but it allows me to create enumeration of strings.
	 * @param name
	 * Name of searched parameter.
	 * @return
	 * Such parameter if found. Otherwise throws exception.
	 */
	public static Commands fromName(final String name) {
		for(final Commands c : Commands.values()) {
			if(c.toString().equals(name))	//NOPMD
				return c;
		}
		
		return null;
	}
}
