package org.group.poker.server;

import java.io.BufferedReader;
import java.io.IOException;

public class PipeRead extends Thread {

	private BufferedReader _reader;
	private ConnectionPipe _pipe;
	
	public PipeRead(BufferedReader br, ConnectionPipe cp) {
		_pipe = cp;
		_reader = br;
	}
	
	@Override
	public void run() {
		while(true) {
			String line;
			try {
				line = _reader.readLine();
				if(line == null) {
					_pipe.finish();
					break;
				}
				String ln = _pipe.parse(line);
				_pipe.send_message(ln);
			} catch (IOException e) {
				Server.log("Unable to parse command!");
				_pipe.finish();
				break;
			}
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// NULL
			}
		}
		
	}
	
}
