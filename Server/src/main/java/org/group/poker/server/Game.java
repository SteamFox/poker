package org.group.poker.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import org.group.poker.croupier.Card;
import org.group.poker.croupier.Deck;
import org.group.poker.croupier.Player;
import org.group.poker.croupier.Table;
import org.group.poker.croupier.OwnedHand;
import org.group.poker.croupier.Helper;
import org.group.poker.server.exception.CroupierPlayerExistenceException;
import org.group.poker.server.exception.GameInvalidNumberOfPlayersException;

public class Game extends Thread {
	// FIELDS
	
	private Croupier _croupier;
	private Table _table;
	private List<User> _users;
	private Map<Integer, List<Card>> _cards;
	private List<Card> _publicCards;
	private boolean _gameInProgess;
	
	private Stack<Pot> _pots;
	/**
	 * Round 0 - place blind bets.
	 * Round 1 - 3 cards visible.
	 * Round 2 - 4 cards visible.
	 * Round 3 - 5 cards visible.
	 */
	private int _roundNo;
	private int _activePlayer;
	private int _dealer;
	private float _totalBet;
	
	private GameMode _mode;
	private float _fixedAmount;
	private int _maxRaises;
	
	private boolean _next;
	
	public static final String CroupierName = "Croupier";
	
	//C's & D's
	
	public Game(Croupier croupier,
			GameMode mode, 
			float fixedAmount, 
			int maxRaises) {
		_gameInProgess = false;
		set_users(new ArrayList<User>());
		set_croupier(croupier);
		set_mode(GameMode.NOLIMIT);
		_mode = mode;
		_maxRaises = maxRaises;
		_fixedAmount = fixedAmount;
	}
	
	// GET
	
	public Croupier get_croupier() {
			return _croupier;
	}

	public List<Card> get_publicCards() {
			return _publicCards;
	}

	public Map<Integer, List<Card>> get_cards() {
		return _cards;
	}

	public List<User> get_users() {
			return _users;
	}
	
	public Table get_table() {
			return _table;
	}
	
	public Stack<Pot> get_pots() {
			return _pots;
	}
	
	public Pot get_pot() {
			return get_pots().peek();
	}
	
	public int get_activePlayer() {
		return _activePlayer % get_users().size();
	}
	
	public User get_activeUser() {
			return get_users().get(get_activePlayer());
	}
	
	public int get_dealer() {
		return _dealer;
	}
	
	public User get_dealerUser() {
			return get_users().get(get_dealer());
	}
	
	public int get_smallBlind() {
		return ((_dealer + 1) % get_users().size());
	}
	
	public User get_smallBlindUser() {
			return get_users().get(get_smallBlind());
	}
	
	public int get_bigBlind() {
		return ((get_smallBlind() + 1) % get_users().size());
	}

	public User get_bigBlindUser() {
			return get_users().get(get_bigBlind());
	}
	
	public float get_totalBet() {
		return _totalBet;
	}
	
	public int get_roundNumber() {
		return _roundNo;
	}
	
	public List<User> get_activeUsers() {
		List<User> active = new ArrayList<User>();
		synchronized (_users) {
			for(int i =0; i < get_users().size(); i++) {
				User user = get_users().get(i);
				if(!user.get_lock() && !user.get_waiting())
					active.add(user);
			}
		}
		return active;
	}

	public GameMode get_mode() {
		return _mode;
	}
	
	public float get_fiexedAmount() {
		return _fixedAmount;
	}

	public int get_maxRaises() {
		return _maxRaises;
	}
	
	// SET
	
	public void set_maxRaises(int maxRaises) {
		_maxRaises = maxRaises;
	}
	
	public void set_fixedAmount(float amount) {
		_fixedAmount = amount;
	}

	public void set_mode(GameMode mode) {
		_mode = mode;
	}

	private void set_table(Table table) {
			_table = table;
	}

	private void set_cards(Map<Integer, List<Card>> cards) {
		_cards = cards;
	}

	private void set_publicCards(List<Card> publicCards) {
		_publicCards = publicCards;
	}

	public void set_croupier(Croupier croupier) {
		_croupier = croupier;
	}

	private void set_users(List<User> users) {
		_users = users;
	}
	
	private void set_pots(Stack<Pot> pots) {
		_pots = pots;
	}
	
	public void set_activePlayer(int activePlayer) {
		_activePlayer = activePlayer;
	}
	
	public void pick_dealer() {
		Random rand = new Random();
		_dealer = rand.nextInt(get_users().size());
	}
	
	public void increment_dealer() {
		_dealer = (_dealer + 1) % get_users().size();
	}
	
	public void set_roundNumber(int roundNo) {
		_roundNo = roundNo;
	}
	
	public int increment_round() {
		return ++_roundNo;
	}
	
	private void set_totalBet(float totalBet) {
		_totalBet = totalBet;
	}
	
	public void add_toTotalBet(float bet) {
		set_totalBet(get_totalBet() + bet); 
	}
	
	public void push_newPot() {
		List<User> active = get_activeUsers();
		List<Integer> ids = new ArrayList<Integer>();
		
		for(User user : active) {
			ids.add(user.get_id());
		}
		
		get_pots().push(new Pot(ids));
	}
	
	// CORE
	
	@Override
	public void run() {
		boolean failed = false;
		
		try {
			initialize();
		} catch (CroupierPlayerExistenceException e) {
			Server.log("Failed initializing game!");
			failed = true;
		} catch (GameInvalidNumberOfPlayersException e) {
			Server.log("Invalid number of players!");
			failed = true;
		}
		
		if(failed) {
			synchronized(_croupier) {
				_croupier.set_gameInProgress(false);
			}
			end();
			return;
		}
		
		while(_gameInProgess) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// Do nothing
			}
			if(_next) {
				/** 
				 * Check post conditions and end old hand
				 * and start new hand eventually.
				 */
				if(hasGameFinished()) {
					finishHand();
					try {
						start_hand();
					} catch (CroupierPlayerExistenceException e) {
						Server.log("Failed to create next game!");
						return;
					}
				}
				if(hasRoundFinished()) {
					nextRound();
					continue;
				}
				/**
				 * Ask player for an action.
				 */
				ConnectionPipe pipe = get_croupier()
						.get_listener(get_activeUser().get_id());
				synchronized (pipe) {
					if(get_roundNumber() == 0
							&& !get_activeUser().get_visited()) {
						if(get_activePlayer() == get_smallBlind()) {
							pipe.send_message(
									"MESSAGE You have to place"
									+ " small blind bet!");
						}
						else if(get_activePlayer() == get_bigBlind()) {
							pipe.send_message(
									"MESSAGE You have to place"
									+ " big blind bet!");
						}
					}
					pipe.send_message("MESSAGE Your turn!");				
					_next = false;
				}
			}
		}
	}
	
	public boolean hasRoundFinished() {
		synchronized (_users) {			
			float cmp = get_activeUsers().get(0).get_bet();
			
			for(int i = 1; i < get_activeUsers().size(); i++) {
				User user = get_activeUsers().get(i);
				if(cmp != user.get_bet())
					return false;
				if(!user.get_visited())
					return false;
			}
		}
		
		return true;
	}
	
	public void nextRound() {
		_roundNo++;
		set_activePlayer(get_smallBlind());
		
		synchronized (_users) {
			for(User user : _users) {
				user.set_visited(false);
			}
		}
		
		switch(_roundNo) {
		case 1:
			broadcast("MESSAGE Flop");
			break;
		case 2:
			broadcast("MESSAGE Turn");
			break;
		case 3:
		default:
			broadcast("MESSAGE River");
			break;
		}
	}
	
	public void broadcast(String msg) {
		synchronized (_users) {
			for(int i = 0; i < get_activeUsers().size(); i++) {
				User user = get_activeUsers().get(i);
				ConnectionPipe pipe = 
						_croupier.get_listener(user.get_id());
				if(pipe != null)
					pipe.send_message(msg);
			}
		}
	}
	
	public void end() {
		_gameInProgess = false;
	}
	
	public void initialize() throws 
			CroupierPlayerExistenceException, 
			GameInvalidNumberOfPlayersException {
		List<User> readyUsers = 
				_croupier.get_usersWithStatus(Croupier.FLAG_READY);
		if(readyUsers.size() < 2 || readyUsers.size() > 10)
			throw new GameInvalidNumberOfPlayersException();
		
		_gameInProgess = true;
		set_users(readyUsers);
		
		for(User user : get_users()) {
			user.set_playing(true);
			//TODO(dot, priority(high)): Let admin specify amount!
			user.set_walletContents(10.0f);
		}
		
		// Prepare array of names
		String[] names = new String[get_users().size()];
		for(int i =0 ; i < names.length; i++) {
			User user = get_users().get(i);
			if(user != null)
				names[i] = user.get_name();
		}
		
		set_table(new Table(names));
		pick_dealer();
				
		start_hand();
	}

	public void start_hand() throws 
			CroupierPlayerExistenceException {
		_totalBet = 0.0f;
		for ( int i = 0; i < get_users().size(); i++ ) {
			get_users().get(i).set_bet(0);
		}
		set_roundNumber(0);
		increment_dealer();
		set_activePlayer(get_smallBlind());
		
		Card[] user_cards = get_table().StartNewGame(2);
		
		Deck deck = _table.GetDeck();
		
		for(User user : _users) {
			user.set_playing(true);
			user.set_raises(0);
			user.set_visited(false);
			user.set_waiting(false);
			user.unlock();
			user.not_ready();
		}
		
		set_publicCards(Arrays.asList(deck.PassMultipleCardsToPlayer(
				new Player(CroupierName), 5)));
		
		Map<Integer, List<Card>> cards_map =
				new HashMap<Integer, List<Card>>();
		
		for(int i = 0; i < get_users().size(); i++) {
			cards_map.put(
					get_users().get(i).get_id(),
					new ArrayList<Card>());
		}
		
		for(int i = 0; i < user_cards.length; i++) {
			Player player = user_cards[i].GetPlayer();
			if(player == null)
				throw new CroupierPlayerExistenceException();
			User user = _croupier.get_user_s(player.GetName());
			List<Card> cards = cards_map.get(user.get_id());
			cards.add(user_cards[i]);
		}
		
		set_cards(cards_map);
		
		set_pots(new Stack<Pot>());
		push_newPot();
		set_totalBet(0.0f);
		_next = true;
	}

	// METHODS
	
	public void next() {
		_next = true;
		set_activePlayer((get_activePlayer() + 1)
				% get_users().size()); 
	}
	
	// TODO(dot, priority(high)): Give cash only to winners!
	public void finishHand() {
		//
		synchronized (_pots) {
			
			// List<Card> cards = get_cards().get(id);
			// get_publicCards()
			for(int i = 0; i < get_pots().size(); i++) {
				
			}
			
			
			//List<Card> cards = get_cards().get(0) + get_publicCards();
			
			for(int i = 0; i < get_pots().size(); i++) {
				
				
				
				Pot pot = get_pots().get(i);
				List<Integer> players = pot.get_playersInPot();
				float pot_val = pot.get_potValue();
				int size = players.size();
				
				
				List<OwnedHand> hands = new ArrayList<OwnedHand>();
				
				for(int j = 0; j < size; j++) {
					int player_id = players.get(j);
					
					List<Card> cards = new ArrayList<Card>();
					cards.addAll(get_publicCards());
					cards.addAll(get_cards().get(player_id));
					
					OwnedHand h = null;
					//try {
						h = Helper.GetBestHand(cards);
					/*} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					h.setPlayer(player_id);					
					hands.add(h);
				//	User user = get_croupier().get_user(player_id);
				//	user.set_walletContents(user.get_walletContents() +
				//			pot_val / size);
				}
				List<OwnedHand> winners_tmp = Helper.Winners(hands);
				List<Integer> winners = new ArrayList<Integer>();
				for( OwnedHand w : winners_tmp ) {
					winners.add(w.getPlayer());
				}
				
				for(int j=0; j<winners.size(); j++) {
					User user = get_croupier().get_user( winners.get(j) );
					user.set_walletContents( user.get_walletContents() +
										pot_val / winners.size() );
				}
				
			}
		}
	}
	
	public boolean hasGameFinished() {
		if(_roundNo != 3)
			return false;
		if(get_activeUsers().size() == 1)
			return true;
		
		return hasRoundFinished();
	}

	public void raise_pot(Integer id, float amount) {
		get_pot().raise_potValue(amount);		
	}
}
