package org.group.poker.server.exception;

public class CroupierPlayerAddException extends Exception {

	public CroupierPlayerAddException(String string) {
		super(string);
	}

	public CroupierPlayerAddException() {
		super();
	}

	/**
	 * Generated serial ID.
	 */
	private static final long serialVersionUID = 5925693134300125358L;

}
