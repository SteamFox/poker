package org.group.poker.server;

public enum GameMode {
	FIXED,
	POT,
	NOLIMIT
}
