package org.group.poker.server.utility;

/**
 * Counter class. It's method returns unique number every time.
 * @author dotPo
 */
public final class Counter {
	/**
	 * Number buffer.
	 */
	private static int _count = 0;
	
	/**
	 * Only static.
	 */
	private Counter() {}
	
	/**
	 * @return
	 * Next number.
	 */
	public static int getNext() {return ++_count;}
}
