package org.group.poker.server.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.group.poker.server.ConnectionPipe;
import org.group.poker.server.Croupier;
import org.group.poker.server.User;
import org.group.poker.server.exception.CroupierGameAlreadyRunningException;
import org.group.poker.server.exception.CroupierListenerExistenceException;
import org.group.poker.server.exception.CroupierNoGameInProgressException;
import org.group.poker.server.exception.CroupierNotUniqueNameException;
import org.group.poker.server.exception.CroupierPlayerExistenceException;
import org.group.poker.server.utility.Counter;
import org.junit.Before;
import org.junit.Test;

public class CroupierTesterBasic {
	private ConnectionPipe _dummyConnectionPipe;
	private Croupier _croupier;
	private static final String Name = "Dummy";
	
	@Before
	public void setUp() {
		_dummyConnectionPipe = mock(ConnectionPipe.class);
		_croupier = new Croupier();
	}
	
	@Test
	public void Test_WasInitOk() {
		assertNotNull(_croupier.get_listeners());
		assertNotNull(_croupier.get_users());
	}
	
	@Test
	public void Test_CanStartStopInterruptGame() {
		int siz = _croupier.get_usersWithStatus(
				Croupier.FLAG_READY).size();
		Map<Integer, User> users = new HashMap<Integer, User>();
		int id = Counter.getNext();
		User user = new User(id, "Dummy" + id);
		user.ready();
		users.put(id, user);
		id = Counter.getNext();
		user = new User(id, "Dummy" + id);
		user.ready();
		users.put(id, user);
		if(siz < 2)
			_croupier.set_users(users);
		try {
			_croupier.start_game();
		} catch (CroupierGameAlreadyRunningException e) {
			fail();
		}
		assertNotNull(_croupier.get_game());
		assertTrue(_croupier.is_gameInProgress());
		try {
			_croupier.stop_game();
		} catch (CroupierNoGameInProgressException e) {
			fail();
		}
		assertNull(_croupier.get_game());
		assertFalse(_croupier.is_gameInProgress());
		try {
			_croupier.start_game();
		} catch (CroupierGameAlreadyRunningException e) {
			fail();
		}
		assertNotNull(_croupier.get_game());
		assertTrue(_croupier.is_gameInProgress());
		try {
			_croupier.interrupt_game();
		} catch (CroupierNoGameInProgressException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
		assertNull(_croupier.get_game());
		assertFalse(_croupier.is_gameInProgress());
	}
	
	@Test
	public void Test_ListenerManagement() {
		int exist = Counter.getNext();
		int not_exist = Counter.getNext();
		try {
			_croupier.add_listener(exist, _dummyConnectionPipe);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
		assertSame(_croupier.get_listener(exist), _dummyConnectionPipe);
		assertNull(_croupier.get_listener(not_exist));
		try {
			assertSame(_croupier.get_listener_s(exist),
					_dummyConnectionPipe);
		} catch (CroupierListenerExistenceException e1) {
			fail();
		}
		try {
			_croupier.get_listener_s(not_exist);
			fail();
		} catch (CroupierListenerExistenceException e1) {
			// Correct! Listener doesn't exist!
		}
		try {
			_croupier.add_user(exist, Name);
		} catch (CroupierPlayerExistenceException e1) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}
		try {
			_croupier.remove_listener(exist);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
		assertNull(_croupier.get_user(exist));
	}
	
	@Test
	public void Test_UserManagement() {
		int exist = Counter.getNext();
		int not_exist = Counter.getNext();
		try {
			_croupier.add_listener(exist, _dummyConnectionPipe);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
		try {
			_croupier.add_user(exist, Name);
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}
		try {
			_croupier.add_user(exist, Name);
			fail();
		} catch (CroupierPlayerExistenceException e) {
			// Correct! User does exist!
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}
		try {
			_croupier.add_user(not_exist, Name);
			fail();
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			// Correct! Listener doesn't exist!
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}		
		assertNull(_croupier.get_user(not_exist));
		User user = _croupier.get_user(exist);
		assertNotNull(user);
		assertSame(user.get_name(), Name);
		try {
			_croupier.get_user_s(not_exist);
			fail();
		} catch (CroupierPlayerExistenceException e) {
			// Correct! User doesn't exits!
		}
		try {
			user = _croupier.get_user_s(exist);
		} catch (CroupierPlayerExistenceException e) {
			fail();
		}
		assertSame(user.get_name(), Name);
		assertNotNull(_croupier.get_user(Name));
		try {
			user = _croupier.get_user_s(Name);
		} catch (CroupierPlayerExistenceException e) {
			fail();
		}
		assertSame(user.get_name(), Name);
		try {
			_croupier.remove_user(exist);
		} catch (CroupierPlayerExistenceException e) {
			fail();
		}
		try {
			_croupier.remove_user(not_exist);
			fail();
		} catch (CroupierPlayerExistenceException e) {
			// Correct! User doesn't exist!
		}
		try {
			_croupier.remove_listener(exist);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
	}
	
	@Test
	public void Test_CanGetUserGroups() {
		Map<Integer, User> dummyUsers = new HashMap<Integer, User>();
		
		User userA = new User(Counter.getNext(), "DummyA");
		userA.set_playing(true);
		User userB = new User(Counter.getNext(), "DummyB");
		userB.set_waiting(true);
		User userC = new User(Counter.getNext(), "DummyC");
		userC.ready();
		User userD = new User(Counter.getNext(), "DummyD");
		userD.lock();
		
		dummyUsers.put(userA.get_id(), userA);
		dummyUsers.put(userB.get_id(), userB);
		dummyUsers.put(userC.get_id(), userC);
		dummyUsers.put(userD.get_id(), userD);
		
		_croupier.set_users(dummyUsers);
		
		List<User> status_users = _croupier.get_inGameUsers();
		
		assertEquals(status_users.size(), 1);
		assertEquals(status_users.get(0), userA);
		
		status_users = _croupier.get_usersWithStatus(
				Croupier.FLAG_WAITING);
		
		assertEquals(status_users.size(), 1);
		assertEquals(status_users.get(0), userB);
		
		status_users = _croupier.get_usersWithStatus(
				Croupier.FLAG_READY);
		
		assertEquals(status_users.size(), 1);
		assertEquals(status_users.get(0), userC);
		
		status_users = _croupier.get_usersWithStatus(
				Croupier.FLAG_LOCKED);
		
		assertEquals(status_users.size(), 1);
		assertEquals(status_users.get(0), userD);
	}
}
