package org.group.poker.server.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import java.util.HashMap;
import java.util.Map;

import org.group.poker.server.ConnectionPipe;
import org.group.poker.server.Croupier;
import org.group.poker.server.User;
import org.group.poker.server.exception.CroupierGameAlreadyRunningException;
import org.group.poker.server.exception.CroupierInvalidActionException;
import org.group.poker.server.exception.CroupierInvalidAmountException;
import org.group.poker.server.exception.CroupierListenerExistenceException;
import org.group.poker.server.exception.CroupierMaxRaisesReachedException;
import org.group.poker.server.exception.CroupierNoGameInProgressException;
import org.group.poker.server.exception.CroupierNotUniqueNameException;
import org.group.poker.server.exception.CroupierPlayerExistenceException;
import org.group.poker.server.exception.CroupierPlayerLockedException;
import org.group.poker.server.exception.CroupierTooSmallBetException;
import org.group.poker.server.exception.CroupierUnknownException;
import org.group.poker.server.exception.UserInsufficientFoundsException;
import org.group.poker.server.utility.Counter;
import org.junit.Before;
import org.junit.Test;

public class CroupierTesterCommands {
	private Croupier _croupier;
	private ConnectionPipe _dummyConnectionPipe;
	private static final String Name = "Dummy";
	
	private Map<Integer, User> _dummyUsers;
	
	@Before
	public void setUp() {
		_dummyConnectionPipe = mock(ConnectionPipe.class);
		_croupier = new Croupier();
		_dummyUsers = new HashMap<Integer, User>();
		
		// Insert 5 vessels.
		for(int i = 0; i < 5; i++) {
			int id = Counter.getNext();
			_dummyUsers.put(id, new User(id, Name + Integer.toString(id)));
		}
		
		_croupier.set_users(_dummyUsers);
	}
	
	@Test
	public void Test_CanSetReady() {
		int exist = Counter.getNext();
		int not_exist = Counter.getNext();
		try {
			_croupier.add_listener(exist, _dummyConnectionPipe);
		} catch (CroupierListenerExistenceException e1) {
			fail();
		}
		try {
			_croupier.add_user(exist, Name);
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}
		
		try {
			_croupier.ready(exist);
		} catch (CroupierPlayerExistenceException e) {
			fail();
		}
		
		try {
			_croupier.ready(not_exist);
			fail();
		} catch (CroupierPlayerExistenceException e) {
			// Correct! User doesn't exits.
		}
		
		User user = _croupier.get_user(exist);
		assertNotNull(user);
		assertTrue(user.get_ready());
		
		try {
			_croupier.unready(exist);
		} catch (CroupierPlayerExistenceException e1) {
			fail();
		}
		
		assertFalse(user.get_ready());
		
		try {
			_croupier.remove_user(exist);
		} catch (CroupierPlayerExistenceException e1) {
			fail();
		}
		
		try {
			_croupier.remove_listener(exist);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
	}
	
	@Test
	public void Test_CanPlayerBetAndRaise() {
		int exist = Counter.getNext();
		int exist2 = Counter.getNext();
		try {
			_croupier.add_listener(exist, _dummyConnectionPipe);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
		try {
			_croupier.add_user(exist, "A");
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}
		try {
			_croupier.ready(exist);
		} catch (CroupierPlayerExistenceException e1) {
			fail();
		}
		try {
			_croupier.add_listener(exist2, _dummyConnectionPipe);
		} catch (CroupierListenerExistenceException e) {
			fail();
		}
		try {
			_croupier.add_user(exist2, "B");
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (CroupierNotUniqueNameException e) {
			fail();
		}
		try {
			_croupier.ready(exist2);
		} catch (CroupierPlayerExistenceException e1) {
			fail();
		}
		try {
			_croupier.start_game();
		} catch (CroupierGameAlreadyRunningException e) {
			fail();
		}
		User u = _croupier.get_game().get_activeUser();
		float wc = u.get_walletContents();
		float potc = _croupier.get_game().get_totalBet();
		try {
			_croupier.bet(u.get_id(), "5.0");
		} catch (NumberFormatException e) {
			fail();
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierNoGameInProgressException e) {
			fail();
		} catch (CroupierInvalidActionException e) {
			fail();
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (UserInsufficientFoundsException e) {
			fail();
		} catch (CroupierUnknownException e) {
			fail();
		} catch (CroupierPlayerLockedException e) {
			fail();
		}
		assertTrue(u.get_walletContents() == wc - 5.0f);
		assertTrue(u.get_bet() == 5.0f);
		assertTrue(_croupier.get_game().get_totalBet() == potc + 5.0f);
		
		u = _croupier.get_game().get_activeUser();
		wc = u.get_walletContents();
		potc = _croupier.get_game().get_totalBet();
		try {
			try {
				_croupier.raise(u.get_id(), "2.0");
			} catch (CroupierTooSmallBetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fail();
		} catch (NumberFormatException e) {
			fail();
		} catch (CroupierPlayerExistenceException e) {
			fail();
		} catch (CroupierNoGameInProgressException e) {
			fail();
		} catch (CroupierInvalidActionException e) {
			
		} catch (CroupierListenerExistenceException e) {
			fail();
		} catch (UserInsufficientFoundsException e) {
			fail();
		} catch (CroupierUnknownException e) {
			fail();
		} catch (CroupierPlayerLockedException e) {
			
		} catch (CroupierInvalidAmountException e) {
			fail();
		} catch (CroupierMaxRaisesReachedException e) {
			fail();
		}
		assertTrue(u.get_walletContents() == wc);
		assertTrue(u.get_bet() == 0.0f);
		assertTrue(_croupier.get_game().get_totalBet() == potc);
	}
}
