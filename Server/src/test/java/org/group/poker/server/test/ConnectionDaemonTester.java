package org.group.poker.server.test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.group.poker.server.ConnectionDaemon;
import org.group.poker.server.Croupier;
import org.junit.Before;
import org.junit.Test;

public class ConnectionDaemonTester {

	private ServerSocket dummyServer;
	private Socket dummyClient;
	private ConnectionDaemon daemon;
	private OutputStream dummyOut;
	private InputStream dummyIn;
	private Croupier dummyCroupier;
	
	@Before
	public void SetUp() throws IOException
	{
		dummyClient = mock(Socket.class);
		dummyServer = mock(ServerSocket.class);
		dummyOut = mock(OutputStream.class);
		dummyIn = mock(InputStream.class);
		dummyCroupier = mock(Croupier.class);
		
		when(dummyServer.accept()).thenReturn(dummyClient);
		when(dummyClient.getInputStream()).thenReturn(dummyIn);
		when(dummyClient.getOutputStream()).thenReturn(dummyOut);
		
		daemon = new ConnectionDaemon(dummyServer, dummyCroupier);
	}
	
	
	/**
	 * Tests whether connection daemon can handle new socket.
	 */
	@Test
	public void Test_CanDaemonAcceptConnections()
	{
		daemon.handleNewConnection(dummyClient);
		assertTrue(daemon.get_currentConnections().size() > 0);
	}
}
