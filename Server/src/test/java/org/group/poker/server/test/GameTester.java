package org.group.poker.server.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.group.poker.croupier.Card;
import org.group.poker.server.Croupier;
import org.group.poker.server.Game;
import org.group.poker.server.User;
import org.group.poker.server.exception.CroupierGameAlreadyRunningException;
import org.group.poker.server.exception.CroupierNoGameInProgressException;
import org.group.poker.server.exception.CroupierPlayerExistenceException;
import org.group.poker.server.utility.Counter;
import org.junit.Before;
import org.junit.Test;

public class GameTester {
	private Croupier _croupier;
	private Game _game;
	private Map<Integer, User> _users;
	
	public Croupier get_croupier() {
		synchronized(_croupier) {
			return _croupier;
		}
	}
	
	public Game get_game() {
		synchronized(_game) {
			return _game;
		}
	}
	
	public void set_game(Game game) {
		_game = game;
	}
	
	@Before
	public void setUp() {
		_croupier = new Croupier();
		
		_users = new HashMap<Integer, User>();
		
		for(int i = 0; i < 5; i++) {
			Integer id = Counter.getNext();
			_users.put(id,
					new User(id, "Dummy" + Integer.toString(i)));
		}
		
		get_croupier().set_users(_users);
	}
	
	@Test
	public void Test_CanFailToStartGame() {
		for(User user : _users.values()) {
			try {
				get_croupier().unready(user.get_id());
			} catch (CroupierPlayerExistenceException e) {
				fail();
			}
		}
		if(get_croupier()
				.get_usersWithStatus(Croupier.FLAG_READY).size() > 0)
			fail();
		try {
			get_croupier().start_game();
		} catch (CroupierGameAlreadyRunningException e1) {
			fail();
		}
		assertFalse(get_croupier().is_gameInProgress());
	}
	
	@Test
	public void Test_CanStartGame() {
		for(User user : _users.values()) {
			try {
				get_croupier().ready(user.get_id());
			} catch (CroupierPlayerExistenceException e) {
				fail();
			}
		}		
		if(get_croupier()
				.get_usersWithStatus(Croupier.FLAG_READY).size() < 2)
			fail();
		try {
			get_croupier().start_game();
		} catch (CroupierGameAlreadyRunningException e) {
			fail();
		}
		assertTrue(get_croupier().is_gameInProgress());
		try {
			get_croupier().stop_game();
		} catch (CroupierNoGameInProgressException e) {
			fail();
		}
	}
	
	@Test
	public void Test_HasPassedCardsToPlayers() {
		for(User user : _users.values()) {
			try {
				get_croupier().ready(user.get_id());
			} catch (CroupierPlayerExistenceException e) {
				fail();
			}
		}		
		if(get_croupier()
				.get_usersWithStatus(Croupier.FLAG_READY).size() < 2)
			fail();
		try {
			get_croupier().start_game();
		} catch (CroupierGameAlreadyRunningException e) {
			fail();
		}
		assertTrue(get_croupier().is_gameInProgress());
		set_game(get_croupier().get_game());
				
		Map<Integer, List<Card>> cards = get_game().get_cards();
		assertNotNull(cards);
		
		for(User user : _users.values()) {
			assertNotNull(cards.get(user.get_id()));
			assertTrue(cards.get(user.get_id()).size() == 2);
		}
		
		try {
			get_croupier().stop_game();
		} catch (CroupierNoGameInProgressException e) {
			fail();
		}
	}
	
	@Test
	public void Test_DoesHandleSpecialUsersProperly() {
		for(User user : _users.values()) {
			try {
				get_croupier().ready(user.get_id());
			} catch (CroupierPlayerExistenceException e) {
				fail();
			}
		}		
		if(get_croupier()
				.get_usersWithStatus(Croupier.FLAG_READY).size() < 2)
			fail();
		try {
			get_croupier().start_game();
		} catch (CroupierGameAlreadyRunningException e) {
			fail();
		}
		assertTrue(get_croupier().is_gameInProgress());

		set_game(get_croupier().get_game());
		
		assertNotNull(get_game().get_activeUser());
		assertNotNull(get_game().get_dealerUser());
		assertNotNull(get_game().get_smallBlindUser());
		assertNotNull(get_game().get_bigBlindUser());
		int ap = get_game().get_activePlayer();
		int sb = get_game().get_smallBlind();
		int d = get_game().get_dealer();
		int bb = get_game().get_bigBlind();
		int size = get_game().get_users().size();
		assertTrue(ap == sb);
		assertTrue((d + 1) % size == sb);
		assertTrue((sb + 1) % size == bb);
	}
	
	@Test
	public void Test_CanGetActiveUsers() throws 
			CroupierGameAlreadyRunningException {
		Croupier croupier = new Croupier();
		croupier.start_game();
		Game game = croupier.get_game();
		assertNotNull(_users);
		assertSame(_users.size(), 5);
		synchronized (_users) {
			for(User u : _users.values()) {
				assertNotNull(u);
				u.set_waiting(true);
			}
		}
		croupier.set_users(_users);
		List<User> active = game.get_activeUsers();
		assertNotNull(active);
		assertTrue(game.get_activeUsers().size() == 0);
	}
}
