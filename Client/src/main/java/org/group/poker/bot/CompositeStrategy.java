package org.group.poker.bot;

import java.util.List;

public class CompositeStrategy implements IBot
{
	private List<IBot> strategyList;
	
	/**
	 * Adds strategy to 
	 * the list of strategies for bot
	 * 
	 * @param strategy
	 * Strategy to add
	 */
	public void addStrategy(IBot strategy) 
	{
		if(this instanceof CompositeStrategy)
			strategyList.add(strategy);
	}

	@Override
	public void bet() {
		// TODO Auto-generated method stub
		
	}
}
