package org.group.poker.client;

import java.io.BufferedReader;
import java.io.IOException;

public class ClientRead extends Thread {

	private BufferedReader _reader;
	
	public ClientRead(BufferedReader reader) {
		_reader = reader;
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				String line = _reader.readLine();
				if(line == null)
					System.exit(-3);
				parse(line);
			} catch (IOException e) {
				Client.log("Unable to read line");
			}
		}
	}
	
	public void parse(String in) {
		final String cmd[] = in.split(" ", 2);
		
		if(cmd[0].equals("MESSAGE")) {
			if(cmd.length > 1) {
				Client.log(cmd[1], true);
			}
		}
		else if(cmd[0].equals("STATUS")) {
			if(cmd.length < 2) 
				Client.log("Received invalid status message!", true);
			else
				parse_status(cmd[1]);
		}
	}

	private void parse_status(String string) {
		Client.log(string, true);
	}
}
