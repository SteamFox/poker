package org.group.poker.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Scanner;

public class Client {
	
	private Socket _socket;
	private InputStream _socketIn;
	private OutputStream _socketOut;
	private BufferedReader _br;
	private PrintWriter _pw;
	
	private String _host;
	private int _port;
	
	
	public static boolean logAlways = false;
	
	public Client(String host, int port) {
		_host = host;
		_port = port;
	}

	public void set_host(String host, int port) {
		_host = host;
		_port = port;
	}
	
	public void start() {
		try {
			_socket = new Socket(_host, _port);
		} catch (UnknownHostException e) {
			log("Unable to connect, unknown host!", true);
			System.exit(-1);
		} catch (IOException e) {
			log("I/O error occured!", true);
			System.exit(-1);
		}
		
		try {
			_socketIn = _socket.getInputStream();
			_socketOut = _socket.getOutputStream();
		} catch (IOException e) {
			log("Unable to open I/O streams!", true);
			System.exit(-2);
		}
		
		_pw = new PrintWriter(_socketOut, true);
		_br = new BufferedReader(new InputStreamReader(_socketIn));
		
		ClientRead clientRead = new ClientRead(_br);
		clientRead.start();
		ClientWrite clientWrite = new ClientWrite(_pw);
		clientWrite.start();
		
		final Scanner stdin = new Scanner(System.in);
		while(stdin.hasNext()) {
			String line = stdin.nextLine();
			if(line.equals("Exit")) {
				clientWrite.send_message("LEAVE");
				Client.log("Exited!", true);
				System.exit(0);
			}
			else {
				clientWrite.send_message(line);
			}
			try
			{
				Thread.sleep(500);
			} catch (InterruptedException e) {}
		}
		stdin.close();
	}
	
	public static void setLogging(final boolean shouldLog) {
		logAlways = shouldLog;
	}
	
	public static void log(String message, boolean shouldLog) {
		if(logAlways || shouldLog)
			System.out.println((new Date()).toString() 
					+ " : " + message);
	}
	
	public static void log(String message) {
		log(message, false);
	}
}
