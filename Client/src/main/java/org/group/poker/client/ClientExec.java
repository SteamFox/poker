package org.group.poker.client;

public class ClientExec {

	public static void main(String[] args) {
			
		Client c = new Client("localhost", 9090);
		if(args.length > 1) {
			try {
				int port = Integer.parseInt(args[1]);
				c.set_host(args[0], port);
			} catch(NumberFormatException e) {
				Client.log("Unable to parse port!");
			}
		}
		c.start();
	}

}
