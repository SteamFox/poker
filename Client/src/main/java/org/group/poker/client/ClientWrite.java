package org.group.poker.client;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ClientWrite extends Thread {

	private PrintWriter _writer;
	private List<String> _backlog;
	
	public ClientWrite(PrintWriter writer) {
		_writer = writer;
		_backlog = new ArrayList<String>();
	}
	
	@Override
	public void run() {
		while(true) {
			synchronized (_backlog) {
				int size = _backlog.size();
				if(size > 0) {
					for(int it = size - 1; it > -1; it--) {
						String s = _backlog.get(it);
						String[] ns = s.split(" ", 2);
						_writer.println(ns[0].toUpperCase() +
								(ns.length > 1 ? " " + ns[1] : ""));
						_backlog.remove(it);
					}
				}			
			}
		}
	}
	
	public void send_message(String message) {
		synchronized (_backlog) {
			_backlog.add(message);
		}
	}
}
