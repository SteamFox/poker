<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE module PUBLIC "-//Puppy Crawl//DTD Check Configuration 1.3//EN" "http://www.puppycrawl.com/dtds/configuration_1_3.dtd">

<!--
    This configuration file was written by the eclipse-cs plugin configuration editor
-->
<!--
    Checkstyle-Configuration: PokerSimplistic
    Description: 
Simple check style for this poker project.
-->
<module name="Checker">
  <property name="severity" value="warning"/>
  <module name="TreeWalker">
    <module name="MissingOverride"/>
    <module name="JavadocMethod">
      <property name="suppressLoadErrors" value="true"/>
    </module>
    <module name="JavadocType"/>
    <module name="SingleLineJavadoc"/>
    <module name="MemberName">
      <property name="format" value="_[a-z][a-zA-Z0-9]*$"/>
    </module>
    <module name="MethodName"/>
    <module name="UnusedImports"/>
    <module name="GenericWhitespace"/>
    <module name="ModifierOrder"/>
    <module name="DefaultComesLast"/>
    <module name="EqualsAvoidNull"/>
    <module name="FallThrough"/>
    <module name="FinalLocalVariable"/>
    <module name="HiddenField"/>
    <module name="MagicNumber">
      <property name="constantWaiverParentToken" value="TYPECAST,METHOD_CALL,EXPR,ARRAY_INIT,UNARY_MINUS,UNARY_PLUS,ELIST,STAR,ASSIGN,PLUS,MINUS,DIV,LITERAL_NEW"/>
    </module>
    <module name="MissingCtor"/>
  </module>
</module>
