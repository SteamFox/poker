﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class Card : MonoBehaviour {
    public enum Suit
    {
        CLUB=0,
        SPADE=1,
        HEART=2,
        DIAMOND=3
    }

    public enum Weight
    {
        TWO=2,
        THREE=3,
        FOUR=4,
        FIVE=5,
        SIX=6,
        SEVEN=7,
        EIGHT=8,
        NINE=9,
        TEN=10,
        JACK=11,
        QUEEN=12,
        KING=13,
        ACE=14
    }

    private string _name;
    public GameControler gc;

    public void Create()
    {
        _name = "card_back";
    }

    public void Create(Suit suit, Weight weight)
    {
        Debug.Log(suit);
        Debug.Log(weight);
        int nameID = ((int)suit) * 13 + ((int)weight) - 2;
        Debug.Log(nameID);
        _name = "cards_" + nameID.ToString();
    }

    public void LoadSprite()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.sprite = gc.dictSprites[_name];
    }

    public static Suit GetSuitFromChar(char input)
    {
        switch(input)
        {
            case 'C':
                return Suit.CLUB;
            case 'S':
                return Suit.SPADE;
            case 'H':
                return Suit.HEART;
            case 'D':
            default:
                return Suit.DIAMOND;
        }
    }

    void Start () {
        gc = Camera.main.GetComponent<GameControler>();
        gameObject.AddComponent<SpriteRenderer>();
        Create();
    }
}
