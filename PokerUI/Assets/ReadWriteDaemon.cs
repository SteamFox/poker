﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System;
using System.Net;

public class ReadWriteDaemon : MonoBehaviour
{
    private Socket _socket;
    private Thread _write;
    private Thread _read;
    private Thread _heartbeat;
    private Card _card;
    private IList<string> _backlog;
   
    private Card[] _croupierCards;
    private Card[] _yourCards;
    private Card[] _opponentCards;

    public GameControler controler;

    private bool _running;

    public PlayerLog log;
    public string ipadress;
    public int port;
    public string nick;

    private void ReadFromSocket()
    {
        while(_running)
        {
            Thread.Sleep(20);
            byte[] message = new byte[256];
            int size = _socket.Receive(message);
            string line = Encoding.UTF8.GetString(message);
            parse(line);
        }
    }
    
    private void WriteToSocket()
    {
        while (_running)
        {
            Thread.Sleep(20);
            while(_backlog.Count > 0)
            {
                for(int i = _backlog.Count - 1; i > -1; i--)
                {
                    Thread.Sleep(20);
                    _socket.Send(Encoding.UTF8.GetBytes(_backlog[i]));
                    _backlog.RemoveAt(i);
                }
            }
        }
    }

    private void StatusHeartbeat()
    {
        while(_running)
        {
            Thread.Sleep(1000);
            addMessage("STATUS");
        }
    }

    private void parse(string line)
    {
        char[] delim = { ' ' };
        string[] cmd = line.Split(delim, 2);
        if (cmd.Length != 2)
            return;
        if (cmd[0] == "MESSAGE")
            display(cmd[1]);
        else if (cmd[0] == "STATUS")
            displayStatus(cmd[1]);
    }

    public void display(string line)
    {
        log.AddEvent("[" + DateTime.Now.ToString() + "] " + line);
    }

    public void displayStatus(string line)
    {
        
        if (line == "0") return;
        char[] delim = { ';' };
        string[] cmd = line.Split(delim);
        if (cmd.Length != 3 && cmd.Length != 4) return;

        // cmd[0] - user's cards
        char[] innerDelim = { ',' };
        string[] uc = cmd[0].Split(innerDelim);
        if (uc.Length != 2) return;
        for (int i = 0; i < 2; i++)
        {
            if (uc[i] == "[null]")
                _yourCards[i].Create();
            string wght = uc[i][1].ToString();
            wght = wght + (uc[i][2]).ToString();
            _yourCards[i].Create(Card.GetSuitFromChar(uc[i][3]),
                (Card.Weight)int.Parse(wght));
        }

        // cmd[1] Parse list of users.
        uc = cmd[1].Split(innerDelim);
        for(int i = 0; i + 6 < uc.Length; i += 7)
        {
            string full = "";
            if(uc[i + 5] != "N") full += "-> ";
            full += uc[i] + '\n';
            full += uc[i + 1] + '\n';
            full += uc[i + 2] + '\n';
            if (uc[i + 4] != "N") full += "Folded";
            else if (uc[i + 3] != "N") full += "All-In";
            if (uc[i + 6] != "N") full += "Dealer";
            controler.SetLegend(i / 7, full, uc[i] == nick);
        }

        // Parse list of public cards.
        if(cmd.Length > 3)
        {
            uc = cmd[2].Split(innerDelim);

            for(int i = 0; i < uc.Length; i++)
            {
                string wght = uc[i][1].ToString();
                wght = wght + (uc[i][2]).ToString();
                _croupierCards[i].Create(Card.GetSuitFromChar(uc[i][3]),
                    (Card.Weight)int.Parse(wght));
            }

            controler.SetPot(cmd[3]);
        }
        else controler.SetPot(cmd[2]);
    }

    public void addMessage(String line)
    {
        _backlog.Add(line + "\n");
    }

    public void Awake()
    {
        _backlog = new List<string>();
    }

    public void exec()
    {
        _running = true;
        _socket = new Socket(
            AddressFamily.InterNetwork,
            SocketType.Stream,
            ProtocolType.Tcp);
        IPAddress adress = IPAddress.Parse(ipadress);
        IPEndPoint remote = new IPEndPoint(adress, port);
        Debug.Log("Connecting to remote server!");
        _socket.Connect(remote);
        _read = new Thread(ReadFromSocket);
        _read.Start();
        _write = new Thread(WriteToSocket);
        _write.Start();
        _heartbeat = new Thread(StatusHeartbeat);
        _heartbeat.Start();

        _yourCards = new Card[]
        {
            new GameObject("User1").AddComponent<Card>(),
            new GameObject("User2").AddComponent<Card>()
        };
        _croupierCards = new Card[]
        {
            new GameObject("Croupier1").AddComponent<Card>(),
            new GameObject("Croupier2").AddComponent<Card>(),
            new GameObject("Croupier3").AddComponent<Card>(),
            new GameObject("Croupier4").AddComponent<Card>(),
            new GameObject("Croupier5").AddComponent<Card>()
        };
        

        for(int i = 0; i < _yourCards.Length; i++)
        {
            _yourCards[i].transform.position = new Vector2(-8 + (i * 1.2f), -0.5f);
        }
        for(int i = 0; i < _croupierCards.Length; i++)
        {
            _croupierCards[i].transform.position =
                new Vector2(-2.4f + (i * 1.2f), 1.5f);
        }


        addMessage("JOIN " + nick);
        Thread.Sleep(100);
        addMessage("READY");
        timer = 0.0f;
    }

    private float timer;

    public void Update()
    {
        timer += Time.deltaTime;
        if (timer > 1.0f)
        {
            foreach (Card card in _croupierCards)
            {
                card.LoadSprite();
            }
            foreach (Card card in _yourCards)
            {
                card.LoadSprite();
            }
            timer = 0;
        }
    }

    public void OnDestroy()
    {
        _running = false;
        _socket.Shutdown(SocketShutdown.Both);
        _socket.Close();
    }

}
