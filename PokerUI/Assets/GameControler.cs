﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameControler : MonoBehaviour
{
    public UnityEngine.UI.Text host;
    public UnityEngine.UI.Text port;
    public UnityEngine.UI.Text nick;
    public UnityEngine.UI.Text amt;
    public UnityEngine.UI.Text pot;
    public GameObject legendBox;
    public PlayerLog log;
    public GameObject gui;
    public GameObject game;

    public GameObject[] legends;
    private string[] _legendTextes;
    private string[] _legendOldTextes;
    private bool[] _legendIsUser;
    private string _potText;
    private string _potOldText;

    private ReadWriteDaemon rw;


    public Dictionary<string, Sprite> dictSprites = 
        new Dictionary<string, Sprite>();

    void Start()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("cards");

        foreach (Sprite sprite in sprites)
        {
            Debug.Log(sprite.name);
            dictSprites.Add(sprite.name, sprite);
        }

        Sprite back = Resources.Load<Sprite>("card_back");
        dictSprites.Add(back.name, back);

        for (int i = 0; i < legends.Length; i++)
        {
            legends[i].transform.SetParent(game.transform);
        }

        _potOldText = pot.text;
        _legendTextes = new string[legends.Length];
        _legendOldTextes = new string[legends.Length];
        _legendIsUser = new bool[legends.Length];
    }

    public void Update()
    {
        if(_potOldText != _potText)
        {
            pot.text = _potText;
            _potOldText = _potText;
        }
        for(int i = 0; i < _legendTextes.Length; i++)
        {
            if(_legendOldTextes[i] != _legendTextes[i])
            {
                legends[i].GetComponent<UnityEngine.UI.Text>().text = _legendTextes[i];
                _legendOldTextes[i] = _legendTextes[i];
                legends[i].GetComponent<UnityEngine.UI.Text>().color = (_legendIsUser[i] ? Color.red : Color.white);
            }
        }
    }

    public void Connect()
    {
        game.SetActive(true);
        gui.SetActive(false);
        GameObject daemon = new GameObject("Daemon");
        daemon.transform.parent = transform;
        rw = daemon.AddComponent<ReadWriteDaemon>();
        rw.ipadress = host.text;
        rw.port = int.Parse(port.text);
        rw.log = log;
        rw.nick = nick.text;
        rw.controler = this;
        rw.exec();
    }

    public void Bet()
    {
        rw.addMessage("BET " + amt.text);
    }

    public void Raise()
    {
        rw.addMessage("RAISE " + amt.text);
    }

    public void Check()
    {
        rw.addMessage("CHECK");
    }

    public void Call()
    {
        rw.addMessage("CALL");
    }

    public void Fold()
    {
        rw.addMessage("FOLD");
    }

    public void Allin()
    {
        rw.addMessage("ALLIN");
    }

    public void SetLegend(int no, string text, bool isClient)
    {
        _legendTextes[no] = text;
        _legendIsUser[no] = isClient;
    }

    public void SetPot(string text)
    {
        _potText = text;
    }
}
