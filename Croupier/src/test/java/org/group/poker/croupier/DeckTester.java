package org.group.poker.croupier;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class DeckTester 
{
	private Deck _testingObejct1;
	
	@Before
	public void SetUp()
	{
		_testingObejct1 = new Deck();
	}
	
	@After
	public void TerrDown()
	{
		
	}
	
	/**
	 * NOTE: This test is unreliable, since there is chance that shuffle
	 * will return same deck.
	 */
	@Ignore
	@Test
	public void Test_DoesShuffle()
	{
		Deck temp = new Deck();
		temp.Shuffle();
		
		int count = 0;
		for(int i = 0; i < _testingObejct1.GetCards().size(); i++)
		{
			if(temp.GetCards().get(i) == _testingObejct1.GetCards().get(i)) {
				count++;
			}
		}
		assertSame(count, 52);
	}

	@Test
	public void Test_DoesPassCardsToPlayer()
	{
		Player tempPlayer = new Player("Miriam");
		_testingObejct1.PassMultipleCardsToPlayer(tempPlayer, 5);
		
		int count = 0;
		for(Card c : _testingObejct1.GetCards())
		{
			if(c.GetPlayer() != null && 
					c.GetPlayer().GetName() == "Miriam")
			{
				count++;
			}
		}
		assertSame(5, count);
	}
	
	@Test
	public void Test_DoesResetOwnership()
	{
		_testingObejct1.ResetOwnership();
		for(Card card : _testingObejct1.GetCards())
		{
			assertNull(card.GetPlayer());
		}
	}
	
	@Test
	public void DoesHandCardsProperly()
	{
		Deck d = new Deck();
		Player[] tmp_p = {new Player("Dominic"), new Player("Henry")};
		Card[] handed = d.HandCards(tmp_p, 2);
		assertSame(handed.length, 4);
		
		String[] cmp_to = {"Dominic", "Dominic", "Henry", "Henry"};
		Arrays.sort(cmp_to);
		
		//If it throws, error
		int it = 0;
		String[] result_names = new String[4];
		for(Card c : handed)
		{
			result_names[it++] = c.GetPlayer().GetName();
		}
		
		Arrays.sort(result_names);
		
		for(int i = 0; i < 4; i++)
		{
			assertSame(result_names[i], cmp_to[i]);			
		}
	}
}
