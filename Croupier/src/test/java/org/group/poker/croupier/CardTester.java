package org.group.poker.croupier;

import static org.junit.Assert.*;

import org.group.poker.croupier.Card;
import org.group.poker.croupier.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CardTester
{
	Card _testingObject1;
	
	@Before
	public void SetUp()
	{
		_testingObject1 = new Card(7, Card.Suits.SPADE);
	}
	
	@After
	public void TearDown()
	{
		
	}
	
	@Test
	public void Test_IsDescFormatOK()
	{
		StringBuilder dest = new StringBuilder();
		dest.append("[7 ");
		dest.append(CardHelper.GetSymbol(Card.Suits.SPADE));
		dest.append("]");
		assertEquals(dest.toString(), _testingObject1.GetDesc());
	}
	
	@Test
	public void Test_CanGetSetPlayer()
	{
		assertTrue(_testingObject1.GetPlayer() == null);
		Player marvin = new Player("Marvin");
		assertSame(
				_testingObject1.SetPlayer(marvin)
				.GetPlayer().GetName(),
				"Marvin");
	}
}
