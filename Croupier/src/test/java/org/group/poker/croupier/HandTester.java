package org.group.poker.croupier;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.group.poker.croupier.Card;
import org.group.poker.croupier.Hand;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class HandTester {
	private Hand handTest;
	
	@Before
	public void SetUp()
	{
		List<Card> cards = new ArrayList<Card>();
		for(int i=2; i<=6; i++)
		{
			cards.add(new Card(i, Card.Suits.SPADE));
		}
		
		try
		{
			handTest = new Hand(cards);
		} catch (Exception e)
		{
			System.out.println("Exception: " + e.getMessage());
		}
	}
	
	@After
	public void TearDown()
	{
		
	}
	
	@Test
	public void Test_HistogramIsWorking()
	{
		
	}
	
	@Test
	public void Test_HandTypeIsFullHouse()
	{
		//List<Card> c = new ArrayList<Card>(cards);
		//c.append(7, Card.Suits.SPADE);
	}
	
	@Test
	public void Test_HandTypeIsHighCard()
	{
		//List<Card> cards;
		//cards = [];
	}
	
	@Test
	public void Test_HandTypeIsFlush()
	{
		//List<Card> cards;
		//cards = [];
	}
	
	@Test
	public void Test_HandTypeIsStraightFlush()
	{
		//List<Card> cards;
		//cards = [];
	}
	
}
