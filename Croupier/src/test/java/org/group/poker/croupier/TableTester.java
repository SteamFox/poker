package org.group.poker.croupier;

import static org.junit.Assert.*;

import org.group.poker.croupier.Card;
import org.group.poker.croupier.Table;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TableTester 
{
	private Table _testingObejct1;
	
	@Before
	public void SetUp()
	{
		_testingObejct1 = new Table(
				new String[] {"Joshua", "Ronald", "John"});
	}
	
	@After
	public void TearDown()
	{
		
	}
	
	@Test
	public void Test_DoesGameStartProperly()
	{
		assertNotNull(_testingObejct1.StartNewGame(5));
	}
	
	@Test
	public void Test_DoesCrashWhenTooManyCards()
	{
		assertNull(_testingObejct1.StartNewGame(25));
	}
	
	@Test
	public void Test_DoesEachPlayerGetSameAmount()
	{
		_testingObejct1.StartNewGame(3);
		int numOP = _testingObejct1.GetPlayers().length;
		int[] amount = new int[numOP];
		for(int i = 0; i < numOP; i++)
		{
			for(Card card : _testingObejct1.GetDeck().GetCards())
			{
				if(card.GetPlayer() != null)
				if(card.GetPlayer().GetName() ==
						_testingObejct1.GetPlayers()[i].GetName())
				{
					amount[i]++;
				}
			}
		}
		
		for(int i : amount)
		{
			assertSame(amount[0], i);
		}
	}
}
 