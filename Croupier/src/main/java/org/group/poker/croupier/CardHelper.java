package org.group.poker.croupier;

//TODO: Make it more usefull, maye an enum which can convert full
// card description into symbols and vice versa.
public class CardHelper
{
	// STATIC

	/**
	 * Get English name of card's symbol.
	 * @param symbol
	 * Symbol to convert.
	 * @return
	 * Card name, either Spade, Heart, Diamond, Club or null
	 */
	public static Card.Suits GetName(char symbol)
	{
		switch(symbol)
		{
		case 'S':
			return Card.Suits.SPADE;
		case 'D':
			return Card.Suits.DIAMOND;
		case 'C':
			return Card.Suits.CLUB;
		case 'H':
		default:
			return Card.Suits.HEART;
		}
	}

	/**
	 * Convert English card symbol name to Unicode character
	 * which represents that symbol.
	 * @param name
	 * English name of symbol.
	 * @return
	 * Unicode character representing given name or (char)0.
	 */
	public static char GetSymbol(Card.Suits name)
	{
		switch(name)
		{
		case SPADE:
			return 'S';
		case DIAMOND:
			return 'D';
		case CLUB:
			return 'C';
		case HEART:
		default:
			return 'H';
		}
	}

}
