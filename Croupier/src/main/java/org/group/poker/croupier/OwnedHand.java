package org.group.poker.croupier;

import java.util.List;

public class OwnedHand extends Hand
{
	
	public OwnedHand( List<Card> _cards )
	{
		 super( _cards );
	}
	
	Integer playerID = -1;
	public void setPlayer(Integer i ) { playerID = i;}
	public Integer getPlayer() { return playerID; }
}


