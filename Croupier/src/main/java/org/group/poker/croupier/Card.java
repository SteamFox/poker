package org.group.poker.croupier;


public class Card implements Comparable<Card>
{
	public enum Suits {
		CLUB, DIAMOND, HEART, SPADE
	}

	// FIELDS

	private final Integer value;
	private final Suits suit;

	private Player player;

	// C's & D's

	public Card(Integer _value, Suits _suit)
	{
		suit = _suit;
		value = _value;
	}

	// GETTERS

	public Integer GetValue() {return value;}
	public Suits GetSuit() {return suit;}
	public Player GetPlayer() {return player;}

	// SETTERS

	/**
	 * Sets card owner.
	 * @param player
	 * Player's name.
	 * @return
	 * this.
	 */
	public Card SetPlayer(Player _player)
	{
		player = _player;
		return this;
	}

	// METHODS

	/**
	 * Get card description. Result is like [7 S].
	 * @return
	 * Description of a card.
	 */
	public String GetDesc()
	{
		final StringBuilder retr = new StringBuilder();
		retr.append('[');
		retr.append(value);
		retr.append(' ');
		retr.append(CardHelper.GetSymbol(suit));
		retr.append(']');
		return retr.toString();
	}

	/**
	 * Resets ownership.
	 * NOTE: this functionality is tested from Deck class tester.
	 * @return
	 * this
	 */
	public Card Reset()
	{
		player = null;
		return this;
	}

	/**
	 * Compares only values of cards.
	 * Value like "A" or "9"
	 * @return
	 * 0 if values are equal
	 * >0 if card2 has lesser value
	 * <0 if card2 has bigger value
	 */

	 public int compareTo( Card c )
	 {
		 return GetValue().compareTo(c.GetValue());
	 }


}
