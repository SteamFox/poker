package org.group.poker.croupier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck
{
	// FIELDS

	private List<Card> _cards;

	// C's & D's

	public Deck()
	{
		_cards = new ArrayList<Card>();

		char[] suits = {'S', 'C', 'H', 'D'};

		for(int i = 2; i<=14; i++)
		{
			for(char col : suits)
			{
				_cards.add(new Card(i, CardHelper.GetName(col)));
			}
		}
	}

	// GETTERS

	public List<Card> GetCards() {return _cards;}

	// METHODS

	/**
	 * Prints deck to System.out.
	 */
	public void PrintDeck()
	{
		for(Card card : _cards)
		{
			if(card.GetPlayer() != null)
				System.out.print(card.GetDesc() +
					' ' + card.GetPlayer().GetName() + '\n');
		}
	}

	/**
	 * Shuffles the deck.
	 */
	public void Shuffle()
	{
		Collections.shuffle(_cards);
	}

	/**
	 * Passes one card to player.
	 * @param player
	 * Target player.
	 * @return
	 * Card that was passed or null if failed to do so.
	 */
	public Card PassCardToPlayer(Player player)
	{
		for(Card card : _cards)
		{
			if(card.GetPlayer() == null) return card.SetPlayer(player);
		}
		return null;
	}

	/**
	 * Passes number of cards to player.
	 * @param player
	 * Target player.
	 * @param number
	 * Amount of cards to pass.
	 * @return
	 * Table of all cards passed or null if failed to pass all of them.
	 */
	public Card[] PassMultipleCardsToPlayer(Player player, int number)
	{
		List<Card> retr = new ArrayList<Card>();
		int passed = 0;
		for(Card card : _cards)
		{
			if(card.GetPlayer() == null)
			{
				retr.add(card.SetPlayer(player));
				passed++;
			}
			if(passed == number) break;
		}
		if(passed == number)
			return (Card[]) retr.toArray(new Card[retr.size()]);
		return null;
	}

	/**
	 * Pass number of cards to each player.
	 * @param players
	 * Target players.
	 * @param howMany
	 * Amount of cards each one will get.
	 * @return
	 * this or null on fail.
	 */
	public Card[] HandCards(Player[] players, int howMany)
	{
		Card[] retr = new Card[howMany * players.length];
		int it = 0;
		for(Player player : players)
		{
			Card[] tmp = PassMultipleCardsToPlayer(player, howMany);
			if(tmp == null)
				return null;
			
			for(int i = 0; i < tmp.length; i++, it++)
			{
				retr[it] = tmp[i];
			}
		}
		
		return retr;
	}

	/**
	 * Resets ownership of all cards.
	 * @return
	 * this
	 */
	public Deck ResetOwnership()
	{
		for(Card c : _cards)
		{
			if(c.GetPlayer() != null) c.Reset();
		}
		return this;
	}
}
