package org.group.poker.croupier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Helper {

	// TODO: zwracac graczy?
	public static List<OwnedHand> Winners( List<OwnedHand> hands )
	{
		Collections.sort(hands);
		Collections.reverse(hands);
		int i;
		for( i=1; i<hands.size(); i++ )
		{
			if( hands.get(i).compareTo(hands.get(0)) < 0 )
			{
				break;
			}
		}
		
		return hands.subList(0, i);
	}
	
  	public static OwnedHand GetBestHand( List<Card> cards )
  	{
  		if( cards.size() <= 5 )
  		{	
  			return new OwnedHand(cards);
  		}
  		else
  		{
  			List<OwnedHand> hands = new ArrayList<OwnedHand>();
  			for(int i=0; i < cards.size(); i++)
  			{
  				List<Card> c = new ArrayList<Card>(cards);
  				c.remove(i);
  				hands.add(GetBestHand(c));
  			}
  			return Winners(hands).get(0);
  		}
  	}
  		

}
