package org.group.poker.croupier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.TreeSet;


public class Hand implements Comparable<Hand> {

  public enum HandType
  {
    HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE_OF_A_KIND, STRAIGHT,
    FLUSH, FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH
  }

  private List<Card> cards;
  private Integer[] histogram;
  private HandType type;
  private List<Card> kickers;
  
  public Hand( List<Card> _cards )
  {
	  if(_cards.size() != 5)
	  {
		  System.out.println("ERRROR Bad hand size: " + _cards.size() );
	  }
	  this.cards = _cards;
	  Collections.sort(_cards);
	  
	  histogram = new Integer[15];
	  for ( int i = 0; i < 15; i++ )
	  {
		  histogram[i] = 0;
	  }
		
	  for ( Card c : _cards )
	  {
		  histogram[c.GetValue()]++;
	  }
	  
	  kickers = new ArrayList<Card>( new TreeSet<Card>( cards ).descendingSet() );
	  
	  type = SetType();
	  
	}
  



	/**
	 * Sets type of a hand. Uses histogram
	 * to determine type of hand.
	 * @return
	 * Type of a hand.
	 */
  	private HandType SetType()
  	{
		List<Integer> histogram_ = new ArrayList<Integer>(Arrays.asList(histogram));
		Collections.reverse(histogram_);
		
		HandType _type = HandType.HIGH_CARD;
		
		if(histogram_.get(0) == 4)
		{
		  _type = HandType.FOUR_OF_A_KIND;
		}
		
		if(histogram_.get(0) == 3)
		{
		  if(histogram_.get(1) == 2)
		  {
		    _type = HandType.FULL_HOUSE;
		  }
		  else
		  {
		    _type = HandType.THREE_OF_A_KIND;
		  }
		}
		
		if(histogram_.get(0) == 2)
		{
		  if(histogram_.get(1) == 2)
		  {
		    _type = HandType.TWO_PAIR;
		  }
		  else
		  {
		    _type = HandType.ONE_PAIR;
		  }
		}
		
		Boolean flush = true;
		Boolean straight = false;
				
		
		for(int i = 1; i < cards.size(); i++)
		{
		  if(cards.get(i).GetSuit() != cards.get(i-1).GetSuit()) {
		    flush = false;
		    break;
		  }
		}
		
		if( kickers.size() == 5 && kickers.get(0).GetValue() - kickers.get(kickers.size()-1).GetValue() == 4 ) {
		  straight = true;
		}
		
		if(flush && straight)
		{
		  _type = HandType.STRAIGHT_FLUSH;
		}
		
		if(flush && (_type.ordinal()<HandType.FLUSH.ordinal()))
		{
		  _type = HandType.FLUSH;
		}
		
		if(straight && (_type.ordinal()<HandType.STRAIGHT.ordinal()))
		{
		  _type = HandType.STRAIGHT ;
		} 
		
		return _type;
  	}
  	
	/**
	 * Gets type of a hand.
	 * @return
	 * Type of a hand.
	 */
  	public HandType GetType()
  	{
  		return type;
  	}
  	
	/**
	 * Gets cards, which decide about winning.
	 * @return
	 * Kickers (side cards).
	 */
  	public List<Card> GetKickers()
  	{
  		return kickers;
  	}
  
  	public int compareTo( Hand h )
  	{
  		int tmp = type.compareTo(h.GetType());
  		if ( tmp == 0 )
  		{
  			if( type == HandType.FLUSH && type == HandType.STRAIGHT_FLUSH )
  			{
  				return kickers.get(0).compareTo(h.GetKickers().get(0));
  			}
  			else
  			{
				if( h.GetKickers().size() < kickers.size() )
					return -1;
				if( h.GetKickers().size() > kickers.size() )
					return 1;
  				for( int i = 0; i < kickers.size(); i++ )
  				{
  					int cmp = kickers.get(i).compareTo(h.GetKickers().get(i));
  					if ( cmp != 0 )
  						return cmp;
  				}				
  				return 0;
  			}
		}
		else
		{
			 return tmp;
		}  
  	}


}
